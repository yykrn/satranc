#include "Square.h"

const std::array<Square_t, BOARD_SIZE> Square::index_120_to_64_{
    sq64::NONE, sq64::NONE, sq64::NONE, sq64::NONE, sq64::NONE, sq64::NONE,
    sq64::NONE, sq64::NONE, sq64::NONE, sq64::NONE, sq64::NONE, sq64::NONE,
    sq64::NONE, sq64::NONE, sq64::NONE, sq64::NONE, sq64::NONE, sq64::NONE,
    sq64::NONE, sq64::NONE, sq64::NONE, sq64::A8,   sq64::B8,   sq64::C8,
    sq64::D8,   sq64::E8,   sq64::F8,   sq64::G8,   sq64::H8,   sq64::NONE,
    sq64::NONE, sq64::A7,   sq64::B7,   sq64::C7,   sq64::D7,   sq64::E7,
    sq64::F7,   sq64::G7,   sq64::H7,   sq64::NONE, sq64::NONE, sq64::A6,
    sq64::B6,   sq64::C6,   sq64::D6,   sq64::E6,   sq64::F6,   sq64::G6,
    sq64::H6,   sq64::NONE, sq64::NONE, sq64::A5,   sq64::B5,   sq64::C5,
    sq64::D5,   sq64::E5,   sq64::F5,   sq64::G5,   sq64::H5,   sq64::NONE,
    sq64::NONE, sq64::A4,   sq64::B4,   sq64::C4,   sq64::D4,   sq64::E4,
    sq64::F4,   sq64::G4,   sq64::H4,   sq64::NONE, sq64::NONE, sq64::A3,
    sq64::B3,   sq64::C3,   sq64::D3,   sq64::E3,   sq64::F3,   sq64::G3,
    sq64::H3,   sq64::NONE, sq64::NONE, sq64::A2,   sq64::B2,   sq64::C2,
    sq64::D2,   sq64::E2,   sq64::F2,   sq64::G2,   sq64::H2,   sq64::NONE,
    sq64::NONE, sq64::A1,   sq64::B1,   sq64::C1,   sq64::D1,   sq64::E1,
    sq64::F1,   sq64::G1,   sq64::H1,   sq64::NONE, sq64::NONE, sq64::NONE,
    sq64::NONE, sq64::NONE, sq64::NONE, sq64::NONE, sq64::NONE, sq64::NONE,
    sq64::NONE, sq64::NONE, sq64::NONE, sq64::NONE, sq64::NONE, sq64::NONE,
    sq64::NONE, sq64::NONE, sq64::NONE, sq64::NONE, sq64::NONE, sq64::NONE};
const std::array<Square_t, INTERNAL_BOARD_SIZE> Square::index_64_to_120_{
    sq120::A8, sq120::B8, sq120::C8, sq120::D8, sq120::E8, sq120::F8, sq120::G8,
    sq120::H8, sq120::A7, sq120::B7, sq120::C7, sq120::D7, sq120::E7, sq120::F7,
    sq120::G7, sq120::H7, sq120::A6, sq120::B6, sq120::C6, sq120::D6, sq120::E6,
    sq120::F6, sq120::G6, sq120::H6, sq120::A5, sq120::B5, sq120::C5, sq120::D5,
    sq120::E5, sq120::F5, sq120::G5, sq120::H5, sq120::A4, sq120::B4, sq120::C4,
    sq120::D4, sq120::E4, sq120::F4, sq120::G4, sq120::H4, sq120::A3, sq120::B3,
    sq120::C3, sq120::D3, sq120::E3, sq120::F3, sq120::G3, sq120::H3, sq120::A2,
    sq120::B2, sq120::C2, sq120::D2, sq120::E2, sq120::F2, sq120::G2, sq120::H2,
    sq120::A1, sq120::B1, sq120::C1, sq120::D1, sq120::E1, sq120::F1, sq120::G1,
    sq120::H1};

const std::array<uint8_t, BOARD_SIZE> Square::file_120_{
    file::NONE, file::NONE, file::NONE, file::NONE, file::NONE, file::NONE,
    file::NONE, file::NONE, file::NONE, file::NONE, file::NONE, file::NONE,
    file::NONE, file::NONE, file::NONE, file::NONE, file::NONE, file::NONE,
    file::NONE, file::NONE, file::NONE, file::NONE, file::A,    file::B,
    file::C,    file::D,    file::E,    file::F,    file::G,    file::H,
    file::NONE, file::NONE, file::A,    file::B,    file::C,    file::D,
    file::E,    file::F,    file::G,    file::H,    file::NONE, file::NONE,
    file::A,    file::B,    file::C,    file::D,    file::E,    file::F,
    file::G,    file::H,    file::NONE, file::NONE, file::A,    file::B,
    file::C,    file::D,    file::E,    file::F,    file::G,    file::H,
    file::NONE, file::NONE, file::A,    file::B,    file::C,    file::D,
    file::E,    file::F,    file::G,    file::H,    file::NONE, file::NONE,
    file::A,    file::B,    file::C,    file::D,    file::E,    file::F,
    file::G,    file::H,    file::NONE, file::NONE, file::A,    file::B,
    file::C,    file::D,    file::E,    file::F,    file::G,    file::H,
    file::NONE, file::NONE, file::A,    file::B,    file::C,    file::D,
    file::E,    file::F,    file::G,    file::H,    file::NONE, file::NONE,
    file::NONE, file::NONE, file::NONE, file::NONE, file::NONE, file::NONE,
    file::NONE, file::NONE, file::NONE, file::NONE, file::NONE, file::NONE,
    file::NONE, file::NONE, file::NONE, file::NONE, file::NONE, file::NONE};
const std::array<uint8_t, BOARD_SIZE> Square::rank_120_{
    rank::NONE, rank::NONE, rank::NONE, rank::NONE, rank::NONE, rank::NONE,
    rank::NONE, rank::NONE, rank::NONE, rank::NONE, rank::NONE, rank::NONE,
    rank::NONE, rank::NONE, rank::NONE, rank::NONE, rank::NONE, rank::NONE,
    rank::NONE, rank::NONE, rank::NONE, rank::R8,   rank::R8,   rank::R8,
    rank::R8,   rank::R8,   rank::R8,   rank::R8,   rank::R8,   rank::NONE,
    rank::NONE, rank::R7,   rank::R7,   rank::R7,   rank::R7,   rank::R7,
    rank::R7,   rank::R7,   rank::R7,   rank::NONE, rank::NONE, rank::R6,
    rank::R6,   rank::R6,   rank::R6,   rank::R6,   rank::R6,   rank::R6,
    rank::R6,   rank::NONE, rank::NONE, rank::R5,   rank::R5,   rank::R5,
    rank::R5,   rank::R5,   rank::R5,   rank::R5,   rank::R5,   rank::NONE,
    rank::NONE, rank::R4,   rank::R4,   rank::R4,   rank::R4,   rank::R4,
    rank::R4,   rank::R4,   rank::R4,   rank::NONE, rank::NONE, rank::R3,
    rank::R3,   rank::R3,   rank::R3,   rank::R3,   rank::R3,   rank::R3,
    rank::R3,   rank::NONE, rank::NONE, rank::R2,   rank::R2,   rank::R2,
    rank::R2,   rank::R2,   rank::R2,   rank::R2,   rank::R2,   rank::NONE,
    rank::NONE, rank::R1,   rank::R1,   rank::R1,   rank::R1,   rank::R1,
    rank::R1,   rank::R1,   rank::R1,   rank::NONE, rank::NONE, rank::NONE,
    rank::NONE, rank::NONE, rank::NONE, rank::NONE, rank::NONE, rank::NONE,
    rank::NONE, rank::NONE, rank::NONE, rank::NONE, rank::NONE, rank::NONE,
    rank::NONE, rank::NONE, rank::NONE, rank::NONE, rank::NONE, rank::NONE,
};

const std::array<int8_t, 8> Square::delta_for_knight_{-21, -19, -12, -8,
                                                      8,   12,  19,  21};
const std::array<int8_t, 4> Square::delta_for_rook_{-10, -1, 1, 10};
const std::array<int8_t, 4> Square::delta_for_bishop_{-11, -9, 9, 11};
const std::array<int8_t, 8> Square::delta_for_king_{-11, -10, -9, -1,
                                                    1,   9,   10, 11};

const std::array<int8_t, 8> Square::delta_for_queen_{-11, -10, -9, -1,
                                                     1,   9,   10, 11};
std::array<std::vector<Square_t *>, INTERNAL_BOARD_SIZE>
    Square::move_to_sq_for_knight_;
std::array<std::vector<Square_t>, INTERNAL_BOARD_SIZE>
    Square::index_to_sq_for_knight_;
std::array<std::vector<Square_t *>, INTERNAL_BOARD_SIZE>
    Square::move_to_sq_for_king_;
std::array<std::vector<Square_t>, INTERNAL_BOARD_SIZE>
    Square::index_to_sq_for_king_;

void Square::Init() {
  for (unsigned int j = 0; j < INTERNAL_BOARD_SIZE; ++j) {
    Square_t sq = Square::Conv64To120(j);
    for (unsigned int i = 0; i < 8; ++i) {
      Square_t target_king = sq + Square::delta_for_king_[i];
      Square_t target_knight = sq + Square::delta_for_knight_[i];

      Piece_t piece_king = Board::board_120_[target_king];
      Piece_t piece_knight = Board::board_120_[target_knight];

      if (piece_king != sq120::OUT) {
        Square::move_to_sq_for_king_[j].push_back(
            &Board::board_120_[target_king]);
        Square::index_to_sq_for_king_[j].push_back(target_king);
      }
      if (piece_knight != sq120::OUT) {
        Square::move_to_sq_for_knight_[j].push_back(
            &Board::board_120_[target_knight]);
        Square::index_to_sq_for_knight_[j].push_back(target_knight);
      }
    }
  }
}

bool Square::IsSqAttacked(const Square_t sq) {
  const Square_t t_sq64 = Square::Conv120To64(sq);
  const uint8_t t_side = 1 - Board::side_to_move_;  // karşı taraf
  if (t_side == side::white) {
    if ((Board::board_120_[sq + 11] == piece::w_pawn) ||
        (Board::board_120_[sq + 9] == piece::w_pawn))
      return true;
    if (Board::piece_list_[piece::w_knight].size())
      for (size_t i = 0, size = Square::move_to_sq_for_knight_[t_sq64].size();
           i != size; ++i) {
        Piece_t piece = *Square::move_to_sq_for_knight_[t_sq64][i];
        if (piece == piece::w_knight) return true;
      }
    for (unsigned int i = 0; i < 4; ++i) {
      int8_t inc = Square::delta_for_bishop_[i];
      Square_t index = sq + inc;
      Piece_t piece = Board::board_120_[index];
      if (piece == piece::w_king) return true;
      while (piece == piece::empty) {
        index += inc;
        piece = Board::board_120_[index];
      }
      if ((piece == piece::w_bishop) || (piece == piece::w_queen)) return true;
    }
    for (unsigned int i = 0; i < 4; ++i) {
      int8_t inc = Square::delta_for_rook_[i];
      Square_t index = sq + inc;
      Piece_t piece = Board::board_120_[index];
      if (piece == piece::w_king) return true;
      while (piece == piece::empty) {
        index += inc;
        piece = Board::board_120_[index];
      }
      if ((piece == piece::w_rook) || (piece == piece::w_queen)) return true;
    }
    /*for (unsigned int i =
    0,size=Square::move_to_sq_for_king_[t_sq64].size();
    i != size;
    ++i) {
    Piece_t piece =
    Board::board_120_[Square::move_to_sq_for_king_[t_sq64][i]];
    if (piece == piece::w_king)
    return true;
    }*/
  } else {
    if ((Board::board_120_[sq - 11] == piece::b_pawn) ||
        (Board::board_120_[sq - 9] == piece::b_pawn))
      return true;
    if (Board::piece_list_[piece::b_knight].size())
      for (size_t i = 0, size = Square::move_to_sq_for_knight_[t_sq64].size();
           i != size; ++i) {
        Piece_t piece = *Square::move_to_sq_for_knight_[t_sq64][i];
        if (piece == piece::b_knight) return true;
      }
    for (unsigned int i = 0; i < 4; ++i) {
      int8_t inc = Square::delta_for_bishop_[i];
      Square_t index = sq + inc;
      Piece_t piece = Board::board_120_[index];
      if (piece == piece::b_king) return true;
      while (piece == piece::empty) {
        index += inc;
        piece = Board::board_120_[index];
      }
      if ((piece == piece::b_bishop) || (piece == piece::b_queen)) return true;
    }
    for (unsigned int i = 0; i < 4; ++i) {
      int8_t inc = Square::delta_for_rook_[i];
      Square_t index = sq + inc;
      Piece_t piece = Board::board_120_[index];
      if (piece == piece::b_king) return true;
      while (piece == piece::empty) {
        index += inc;
        piece = Board::board_120_[index];
      }
      if ((piece == piece::b_rook) || (piece == piece::b_queen)) return true;
    }
    /*for (unsigned int i =
    0,size=Square::move_to_sq_for_king_[t_sq64].size();
    i !=size ;
    ++i) {
    Piece_t piece =
    Board::board_120_[Square::move_to_sq_for_king_[t_sq64][i]];
    if (piece == piece::b_king)
    return true;
    }*/
  }

  return false;
}