#include "Move.h"

void Move::PrintCoordNot(const Move_t move) {
  static const std::unordered_map<Square_t, std::string> square_to_str{
      {sq120::A1, "a1"},
      {sq120::B1, "b1"},
      {sq120::C1, "c1"},
      {sq120::D1, "d1"},
      {sq120::E1, "e1"},
      {sq120::F1, "f1"},
      {sq120::G1, "g1"},
      {sq120::H1, "h1"},
      {sq120::A2, "a2"},
      {sq120::B2, "b2"},
      {sq120::C2, "c2"},
      {sq120::D2, "d2"},
      {sq120::E2, "e2"},
      {sq120::F2, "f2"},
      {sq120::G2, "g2"},
      {sq120::H2, "h2"},
      {sq120::A3, "a3"},
      {sq120::B3, "b3"},
      {sq120::C3, "c3"},
      {sq120::D3, "d3"},
      {sq120::E3, "e3"},
      {sq120::F3, "f3"},
      {sq120::G3, "g3"},
      {sq120::H3, "h3"},
      {sq120::A4, "a4"},
      {sq120::B4, "b4"},
      {sq120::C4, "c4"},
      {sq120::D4, "d4"},
      {sq120::E4, "e4"},
      {sq120::F4, "f4"},
      {sq120::G4, "g4"},
      {sq120::H4, "h4"},
      {sq120::A5, "a5"},
      {sq120::B5, "b5"},
      {sq120::C5, "c5"},
      {sq120::D5, "d5"},
      {sq120::E5, "e5"},
      {sq120::F5, "f5"},
      {sq120::G5, "g5"},
      {sq120::H5, "h5"},
      {sq120::A6, "a6"},
      {sq120::B6, "b6"},
      {sq120::C6, "c6"},
      {sq120::D6, "d6"},
      {sq120::E6, "e6"},
      {sq120::F6, "f6"},
      {sq120::G6, "g6"},
      {sq120::H6, "h6"},
      {sq120::A7, "a7"},
      {sq120::B7, "b7"},
      {sq120::C7, "c7"},
      {sq120::D7, "d7"},
      {sq120::E7, "e7"},
      {sq120::F7, "f7"},
      {sq120::G7, "g7"},
      {sq120::H7, "h7"},
      {sq120::A8, "a8"},
      {sq120::B8, "b8"},
      {sq120::C8, "c8"},
      {sq120::D8, "d8"},
      {sq120::E8, "e8"},
      {sq120::F8, "f8"},
      {sq120::G8, "g8"},
      {sq120::H8, "h8"},
  };
  std::stringstream ss;
  ss << square_to_str.at(Move::GetStartSq(move));
  ss << square_to_str.at(Move::GetEndSq(move));
  if (Move::IsPromotionMove(move)) {
    uint8_t p_type = Piece::GetType(Move::GetPromotedPiece(move));
    switch (p_type) {
      case piece_type::queen:
        ss << "q";
        break;
      case piece_type::knight:
        ss << "n";
        break;
      case piece_type::rook:
        ss << "r";
        break;
      case piece_type::bishop:
        ss << "b";
        break;
      default:
        std::cout << "promotion piece error!" << std::endl;
    }
  }

  std::cout << ss.str() << " ";
}

void Move::PrintMove(const Move_t move) {
  std::stringstream ss;
  ss << "piece:" << to_str::pieces[Move::GetMovedPiece(move)] << "\n";

  if (Move::IsCaptureMove(move))
    ss << "captured:" << to_str::pieces[Move::GetCapturedPiece(move)] << "\n";

  ss << "en pas cap:" << (Move::IsEnPasCaptMove(move) ? "yes" : "no") << "\n";
  ss << "pawn 2 sq:" << (Move::IsPawn2SqMove(move) ? "yes" : "no") << "\n";

  ss << "0-0-0:" << (Move::IsCastlQueenMove(move) ? "yes" : "no") << "\n";
  ss << "0-0:" << (Move::IsCastlKingMove(move) ? "yes" : "no") << "\n";

  std::cout << ss.str() << std::endl;
}
Move_t Move::NotaToMove(const std::string &str) {
  static const std::unordered_map<std::string, Square_t> str_to_square{
      {"a1", sq120::A1},
      {"b1", sq120::B1},
      {"c1", sq120::C1},
      {"d1", sq120::D1},
      {"e1", sq120::E1},
      {"f1", sq120::F1},
      {"g1", sq120::G1},
      {"h1", sq120::H1},
      {"a2", sq120::A2},
      {"b2", sq120::B2},
      {"c2", sq120::C2},
      {"d2", sq120::D2},
      {"e2", sq120::E2},
      {"f2", sq120::F2},
      {"g2", sq120::G2},
      {"h2", sq120::H2},
      {"a3", sq120::A3},
      {"b3", sq120::B3},
      {"c3", sq120::C3},
      {"d3", sq120::D3},
      {"e3", sq120::E3},
      {"f3", sq120::F3},
      {"g3", sq120::G3},
      {"h3", sq120::H3},
      {"a4", sq120::A4},
      {"b4", sq120::B4},
      {"c4", sq120::C4},
      {"d4", sq120::D4},
      {"e4", sq120::E4},
      {"f4", sq120::F4},
      {"g4", sq120::G4},
      {"h4", sq120::H4},
      {"a5", sq120::A5},
      {"b5", sq120::B5},
      {"c5", sq120::C5},
      {"d5", sq120::D5},
      {"e5", sq120::E5},
      {"f5", sq120::F5},
      {"g5", sq120::G5},
      {"h5", sq120::H5},
      {"a6", sq120::A6},
      {"b6", sq120::B6},
      {"c6", sq120::C6},
      {"d6", sq120::D6},
      {"e6", sq120::E6},
      {"f6", sq120::F6},
      {"g6", sq120::G6},
      {"h6", sq120::H6},
      {"a7", sq120::A7},
      {"b7", sq120::B7},
      {"c7", sq120::C7},
      {"d7", sq120::D7},
      {"e7", sq120::E7},
      {"f7", sq120::F7},
      {"g7", sq120::G7},
      {"h7", sq120::H7},
      {"a8", sq120::A8},
      {"b8", sq120::B8},
      {"c8", sq120::C8},
      {"d8", sq120::D8},
      {"e8", sq120::E8},
      {"f8", sq120::F8},
      {"g8", sq120::G8},
      {"h8", sq120::H8},
  };

  Move_t move = 0;
  Move_t inp_move = 0;

  Move::SetStartSq(inp_move, str_to_square.at(str.substr(0, 2)));
  Move::SetEndSq(inp_move, str_to_square.at(str.substr(2, 2)));

  if (str.size() == 5) {
    Move::SetPromotedPiece(move, 0xf);
    if (Board::side_to_move_ == side::white) {
      if (str[4] == 'q')
        Move::SetPromotedPiece(inp_move, piece::w_queen);
      else if (str[4] == 'r')
        Move::SetPromotedPiece(inp_move, piece::w_rook);
      else if (str[4] == 'b')
        Move::SetPromotedPiece(inp_move, piece::w_bishop);
      else if (str[4] == 'n')
        Move::SetPromotedPiece(inp_move, piece::w_knight);
    } else {
      if (str[4] == 'q')
        Move::SetPromotedPiece(inp_move, piece::b_queen);
      else if (str[4] == 'r')
        Move::SetPromotedPiece(inp_move, piece::b_rook);
      else if (str[4] == 'b')
        Move::SetPromotedPiece(inp_move, piece::b_bishop);
      else if (str[4] == 'n')
        Move::SetPromotedPiece(inp_move, piece::b_knight);
    }
  }

  Move::SetStartSq(move, 0x7f);
  Move::SetEndSq(move, 0x7f);

  Move_t mv = MoveGen::IsInListByMask(move, inp_move);
  return mv;
}