#ifndef SQUARE_H
#define SQUARE_H

#include "definitions.h"

class Square {
 private:
  static const std::array<Square_t, BOARD_SIZE> index_120_to_64_;
  static const std::array<Square_t, INTERNAL_BOARD_SIZE> index_64_to_120_;

  static const std::array<uint8_t, BOARD_SIZE> file_120_;
  static const std::array<uint8_t, BOARD_SIZE> rank_120_;

 public:
  static const std::array<int8_t, 8> delta_for_knight_;
  static const std::array<int8_t, 4> delta_for_rook_;
  static const std::array<int8_t, 4> delta_for_bishop_;
  static const std::array<int8_t, 8> delta_for_king_;
  static const std::array<int8_t, 8> delta_for_queen_;

  static std::array<std::vector<Square_t *>, 64> move_to_sq_for_knight_;
  static std::array<std::vector<Square_t>, 64> index_to_sq_for_knight_;
  static std::array<std::vector<Square_t *>, 64> move_to_sq_for_king_;
  static std::array<std::vector<Square_t>, 64> index_to_sq_for_king_;

  static bool IsSqAttacked(const Square_t sq);

  static void Init();

  static Square_t CalcSquare120(const uint8_t file, const uint8_t rank) {
    return (10 * rank + 21 + file);
  }

  static Square_t CalcSquare64(const uint8_t file, const uint8_t rank) {
    return (8 * rank + file);
  }

  static uint8_t GetRank(const Square_t sq) {
    // return (sq/10)-2;
    return Square::rank_120_[sq];
  }
  static uint8_t GetFile(const Square_t sq) {
    // return (sq%10)-1;
    return Square::file_120_[sq];
  }
  static Square_t Conv64To120(const Square_t sq) {
    return Square::index_64_to_120_[sq];
  }
  static Square_t Conv120To64(const Square_t sq) {
    return Square::index_120_to_64_[sq];
  }
};

#include "Board.h"

#endif
