#include "MoveGen.h"

/////////////////////////
std::vector<MoveListEntry> MoveGen::movelist_;

std::array<std::array<int64_t, NUM_PIECE_TYPE>, NUM_PIECE_TYPE>
    MoveGen::movcapt_{};

void MoveGen::SetQNormal(const Square_t from, const Square_t to,
                         const Piece_t pc) {
  Move_t move = (from | (to << 7) | (pc << 14));
  int64_t score;
  if (Search::killer_heu_[MakeMove::search_depth_].move1 == move) {
    score = KIL1_SC;
  } else if (Search::killer_heu_[MakeMove::search_depth_].move2 == move) {
    score = KIL2_SC;
  } else {
    score = Search::history_heu_[move & 0x0003ffff];
  }
  MoveGen::movelist_.emplace_back(move, score);
}

inline void MoveGen::SetCNormal(const Square_t from, const Square_t to,
                                const Piece_t pc) {
  Move_t move = (from | (to << 7) | (pc << 14) | (Board::board_120_[to] << 18));
  int64_t score = MoveGen::movcapt_[pc][Board::board_120_[to]];

  MoveGen::movelist_.emplace_back(move, score);
};

void MoveGen::SetWPawnQProm(const Square_t from, const Square_t to) {
  Move_t move = (from | (to << 7) | (piece::w_pawn << 14));
  int64_t score;
  if (Search::killer_heu_[MakeMove::search_depth_].move1 == move) {
    score = KIL1_SC;
  } else if (Search::killer_heu_[MakeMove::search_depth_].move2 == move) {
    score = KIL2_SC;
  } else {
    score = Search::history_heu_[move];
  }

  MoveGen::movelist_.emplace_back((move | (piece::w_queen << 22)), score);
  MoveGen::movelist_.emplace_back((move | (piece::w_rook << 22)), score);
  MoveGen::movelist_.emplace_back((move | (piece::w_bishop << 22)), score);
  MoveGen::movelist_.emplace_back((move | (piece::w_knight << 22)), score);
}

void MoveGen::SetWPawnCProm(const Square_t from, const Square_t to) {
  Move_t move = (from | (to << 7) | (piece::w_pawn << 14) |
                 (Board::board_120_[to] << 18));
  int64_t score =
      MoveGen::movcapt_[piece::w_pawn][Board::board_120_[to]] + PROM_SC;

  MoveGen::movelist_.emplace_back((move | (piece::w_queen << 22)), score);
  MoveGen::movelist_.emplace_back((move | (piece::w_rook << 22)), score);
  MoveGen::movelist_.emplace_back((move | (piece::w_bishop << 22)), score);
  MoveGen::movelist_.emplace_back((move | (piece::w_knight << 22)), score);
}

inline void MoveGen::SetWPawnCEnpas(const Square_t from, const Square_t to) {
  Move_t move = (from | (to << 7) | (piece::w_pawn << 14) |
                 (piece::b_pawn << 18) | 0x10000000);
  int64_t score = MoveGen::movcapt_[piece::w_pawn][piece::b_pawn];
  MoveGen::movelist_.emplace_back(move, score);
}

void MoveGen::SetWPawnQ2Sq(const Square_t from, const Square_t to) {
  Move_t move = (from | (to << 7) | (piece::w_pawn << 14) | (0x20000000));
  int64_t score;
  if (Search::killer_heu_[MakeMove::search_depth_].move1 == move) {
    score = KIL1_SC;
  } else if (Search::killer_heu_[MakeMove::search_depth_].move2 == move) {
    score = KIL2_SC;
  } else {
    score = Search::history_heu_[move & 0x0003ffff];
  }
  MoveGen::movelist_.emplace_back(move, score);
}

void MoveGen::SetWKingCastl() {
  Move_t move =
      (sq120::E1 | (sq120::G1 << 7) | (piece::w_king << 14) | 0x80000000);
  int64_t score;
  if (Search::killer_heu_[MakeMove::search_depth_].move1 == move) {
    score = KIL1_SC;
  } else if (Search::killer_heu_[MakeMove::search_depth_].move2 == move) {
    score = KIL2_SC;
  } else {
    score = Search::history_heu_[move & 0x0003ffff];
  }
  MoveGen::movelist_.emplace_back(move, score);
}

void MoveGen::SetWQueenCastl() {
  Move_t move =
      (sq120::E1 | (sq120::C1 << 7) | (piece::w_king << 14) | 0x40000000);
  int64_t score;
  if (Search::killer_heu_[MakeMove::search_depth_].move1 == move) {
    score = KIL1_SC;
  } else if (Search::killer_heu_[MakeMove::search_depth_].move2 == move) {
    score = KIL2_SC;
  } else {
    score = Search::history_heu_[move & 0x0003ffff];
  }
  MoveGen::movelist_.emplace_back(move, score);
}

///////////////////////////////////////////////////////////////
void MoveGen::SetBPawnQProm(const Square_t from, const Square_t to) {
  Move_t move = (from | (to << 7) | (piece::b_pawn << 14));
  int64_t score;
  if (Search::killer_heu_[MakeMove::search_depth_].move1 == move) {
    score = KIL1_SC;
  } else if (Search::killer_heu_[MakeMove::search_depth_].move2 == move) {
    score = KIL2_SC;
  } else {
    score = Search::history_heu_[move];
  }

  MoveGen::movelist_.emplace_back((move | (piece::b_queen << 22)), score);
  MoveGen::movelist_.emplace_back((move | (piece::b_rook << 22)), score);
  MoveGen::movelist_.emplace_back((move | (piece::b_bishop << 22)), score);
  MoveGen::movelist_.emplace_back((move | (piece::b_knight << 22)), score);
}

void MoveGen::SetBPawnCProm(const Square_t from, const Square_t to) {
  Move_t move = (from | (to << 7) | (piece::b_pawn << 14) |
                 (Board::board_120_[to] << 18));
  int64_t score =
      MoveGen::movcapt_[piece::b_pawn][Board::board_120_[to]] + PROM_SC;

  MoveGen::movelist_.emplace_back((move | (piece::b_queen << 22)), score);
  MoveGen::movelist_.emplace_back((move | (piece::b_rook << 22)), score);
  MoveGen::movelist_.emplace_back((move | (piece::b_bishop << 22)), score);
  MoveGen::movelist_.emplace_back((move | (piece::b_knight << 22)), score);
}

inline void MoveGen::SetBPawnCEnpas(const Square_t from, const Square_t to) {
  Move_t move = (from | (to << 7) | (piece::b_pawn << 14) |
                 (piece::w_pawn << 18) | 0x10000000);
  int64_t score = MoveGen::movcapt_[piece::b_pawn][piece::w_pawn];
  MoveGen::movelist_.emplace_back(move, score);
}

void MoveGen::SetBPawnQ2Sq(const Square_t from, const Square_t to) {
  Move_t move = (from | (to << 7) | (piece::b_pawn << 14) | (0x20000000));
  int64_t score;
  if (Search::killer_heu_[MakeMove::search_depth_].move1 == move) {
    score = KIL1_SC;
  } else if (Search::killer_heu_[MakeMove::search_depth_].move2 == move) {
    score = KIL2_SC;
  } else {
    score = Search::history_heu_[move & 0x0003ffff];
  }
  MoveGen::movelist_.emplace_back(move, score);
}

void MoveGen::SetBKingCastl() {
  Move_t move =
      (sq120::E8 | (sq120::G8 << 7) | (piece::b_king << 14) | 0x80000000);
  int64_t score;
  if (Search::killer_heu_[MakeMove::search_depth_].move1 == move) {
    score = KIL1_SC;
  } else if (Search::killer_heu_[MakeMove::search_depth_].move2 == move) {
    score = KIL2_SC;
  } else {
    score = Search::history_heu_[move & 0x0003ffff];
  }
  MoveGen::movelist_.emplace_back(move, score);
}

void MoveGen::SetBQueenCastl() {
  Move_t move =
      (sq120::E8 | (sq120::C8 << 7) | (piece::b_king << 14) | 0x40000000);
  int64_t score;
  if (Search::killer_heu_[MakeMove::search_depth_].move1 == move) {
    score = KIL1_SC;
  } else if (Search::killer_heu_[MakeMove::search_depth_].move2 == move) {
    score = KIL2_SC;
  } else {
    score = Search::history_heu_[move & 0x0003ffff];
  }
  MoveGen::movelist_.emplace_back(move, score);
}
//////////////////////////////
void MoveGen::AllWPawnMoves() {
  for (size_t i = 0, size = Board::piece_list_[piece::w_pawn].size(); i != size;
       ++i) {
    Square_t sqr = Board::piece_list_[piece::w_pawn][i];
    if (Square::GetRank(sqr) == rank::R7) {
      if (Piece::GetColor(Board::board_120_[sqr - 11]) == side::black)
        MoveGen::SetWPawnCProm(sqr, sqr - 11);
      if (Board::board_120_[sqr - 10] == piece::empty)
        MoveGen::SetWPawnQProm(sqr, sqr - 10);
      if (Piece::GetColor(Board::board_120_[sqr - 9]) == side::black)
        MoveGen::SetWPawnCProm(sqr, sqr - 9);
    } else if ((Board::enpas_sq_ != sq120::NONE) &&
               (Square::GetRank(sqr) == rank::R5)) {
      if ((sqr - 11) == Board::enpas_sq_)
        MoveGen::SetWPawnCEnpas(sqr, sqr - 11);
      else if (Piece::GetColor(Board::board_120_[sqr - 11]) == side::black)
        MoveGen::SetCNormal(sqr, sqr - 11, piece::w_pawn);

      if ((sqr - 9) == Board::enpas_sq_)
        MoveGen::SetWPawnCEnpas(sqr, sqr - 9);
      else if (Piece::GetColor(Board::board_120_[sqr - 9]) == side::black)
        MoveGen::SetCNormal(sqr, sqr - 9, piece::w_pawn);

      if (Board::board_120_[sqr - 10] == piece::empty)
        MoveGen::SetQNormal(sqr, sqr - 10, piece::w_pawn);
    } else if (Square::GetRank(sqr) == rank::R2) {
      if (Board::board_120_[sqr - 10] == piece::empty) {
        MoveGen::SetQNormal(sqr, sqr - 10, piece::w_pawn);

        if (Board::board_120_[sqr - 20] == piece::empty)
          MoveGen::SetWPawnQ2Sq(sqr, sqr - 20);
      }
      if (Piece::GetColor(Board::board_120_[sqr - 11]) == side::black)
        MoveGen::SetCNormal(sqr, sqr - 11, piece::w_pawn);
      if (Piece::GetColor(Board::board_120_[sqr - 9]) == side::black)
        MoveGen::SetCNormal(sqr, sqr - 9, piece::w_pawn);
    } else {
      if (Piece::GetColor(Board::board_120_[sqr - 11]) == side::black)
        MoveGen::SetCNormal(sqr, sqr - 11, piece::w_pawn);
      if (Board::board_120_[sqr - 10] == piece::empty)
        MoveGen::SetQNormal(sqr, sqr - 10, piece::w_pawn);
      if (Piece::GetColor(Board::board_120_[sqr - 9]) == side::black)
        MoveGen::SetCNormal(sqr, sqr - 9, piece::w_pawn);
    }
  }
}
void MoveGen::AllWKnightMoves() {
  for (size_t k = 0, size = Board::piece_list_[piece::w_knight].size();
       k != size; ++k) {
    Square_t t_sq64 =
        Square::Conv120To64(Board::piece_list_[piece::w_knight][k]);
    for (size_t i = 0, size_i = Square::move_to_sq_for_knight_[t_sq64].size();
         i != size_i; ++i) {
      Piece_t t_piece = *Square::move_to_sq_for_knight_[t_sq64][i];
      if (t_piece == piece::empty)
        MoveGen::SetQNormal(Board::piece_list_[piece::w_knight][k],
                            Square::index_to_sq_for_knight_[t_sq64][i],
                            piece::w_knight);
      else if (Piece::GetColor(t_piece) == side::black)
        MoveGen::SetCNormal(Board::piece_list_[piece::w_knight][k],
                            Square::index_to_sq_for_knight_[t_sq64][i],
                            piece::w_knight);
    }
  }
}
void MoveGen::AllWKingMoves() {
  // for(unsigned int k=0,size=Board::piece_list_[piece::w_king].size();
  // k!=size; ++k) {
  unsigned int t_sq64 =
      Square::Conv120To64(Board::piece_list_[piece::w_king][0]);
  for (size_t i = 0, size_i = Square::move_to_sq_for_king_[t_sq64].size();
       i != size_i; ++i) {
    Piece_t piece = *Square::move_to_sq_for_king_[t_sq64][i];
    if (piece == piece::empty)
      MoveGen::SetQNormal(Board::piece_list_[piece::w_king][0],
                          Square::index_to_sq_for_king_[t_sq64][i],
                          piece::w_king);
    else if (Piece::GetColor(piece) == side::black)
      MoveGen::SetCNormal(Board::piece_list_[piece::w_king][0],
                          Square::index_to_sq_for_king_[t_sq64][i],
                          piece::w_king);
  }
  //}
  if ((Board::castling_perm_ & castling::w_king_side) &&
      (Board::board_120_[sq120::F1] == piece::empty) &&
      (Board::board_120_[sq120::G1] == piece::empty) &&
      (!Square::IsSqAttacked(sq120::E1)) && (!Square::IsSqAttacked(sq120::F1)))
    MoveGen::SetWKingCastl();

  if ((Board::castling_perm_ & castling::w_queen_side) &&
      (Board::board_120_[sq120::B1] == piece::empty) &&
      (Board::board_120_[sq120::C1] == piece::empty) &&
      (Board::board_120_[sq120::D1] == piece::empty) &&
      (!Square::IsSqAttacked(sq120::D1)) && (!Square::IsSqAttacked(sq120::E1)))
    MoveGen::SetWQueenCastl();
}
void MoveGen::AllWBishopMoves() {
  for (size_t k = 0, size = Board::piece_list_[piece::w_bishop].size();
       k != size; ++k) {
    Square_t sq = Board::piece_list_[piece::w_bishop][k];
    for (unsigned int i = 0; i < 4; ++i) {
      int8_t inc = Square::delta_for_bishop_[i];
      Square_t index = sq + inc;
      Piece_t piece = Board::board_120_[index];
      while (piece == piece::empty) {
        MoveGen::SetQNormal(sq, index, piece::w_bishop);
        index += inc;
        piece = Board::board_120_[index];
      }
      if (Piece::GetColor(piece) == side::black) {
        MoveGen::SetCNormal(sq, index, piece::w_bishop);
      }
    }
  }
}
void MoveGen::AllWRookMoves() {
  for (size_t k = 0, size = Board::piece_list_[piece::w_rook].size(); k != size;
       ++k) {
    Square_t sq = Board::piece_list_[piece::w_rook][k];
    for (unsigned int i = 0; i < 4; ++i) {
      int8_t inc = Square::delta_for_rook_[i];
      Square_t index = sq + inc;
      Piece_t piece = Board::board_120_[index];
      while (piece == piece::empty) {
        MoveGen::SetQNormal(sq, index, piece::w_rook);
        index += inc;
        piece = Board::board_120_[index];
      }
      if (Piece::GetColor(piece) == side::black) {
        MoveGen::SetCNormal(sq, index, piece::w_rook);
      }
    }
  }
}
void MoveGen::AllWQueenMoves() {
  for (size_t k = 0, size = Board::piece_list_[piece::w_queen].size();
       k != size; ++k) {
    Square_t sq = Board::piece_list_[piece::w_queen][k];
    for (unsigned int i = 0; i < 8; ++i) {
      int8_t inc = Square::delta_for_queen_[i];
      Square_t index = sq + inc;
      Piece_t piece = Board::board_120_[index];
      while (piece == piece::empty) {
        MoveGen::SetQNormal(sq, index, piece::w_queen);
        index += inc;
        piece = Board::board_120_[index];
      }
      if (Piece::GetColor(piece) == side::black) {
        MoveGen::SetCNormal(sq, index, piece::w_queen);
      }
    }
  }
}
////////////////////////////////////////////////////////////
void MoveGen::AllBPawnMoves() {
  for (size_t i = 0, size = Board::piece_list_[piece::b_pawn].size(); i != size;
       ++i) {
    Square_t sqr = Board::piece_list_[piece::b_pawn][i];
    if (Square::GetRank(sqr) == rank::R2) {
      if (Piece::GetColor(Board::board_120_[sqr + 11]) == side::white)
        MoveGen::SetBPawnCProm(sqr, sqr + 11);
      if (Board::board_120_[sqr + 10] == piece::empty)
        MoveGen::SetBPawnQProm(sqr, sqr + 10);
      if (Piece::GetColor(Board::board_120_[sqr + 9]) == side::white)
        MoveGen::SetBPawnCProm(sqr, sqr + 9);
    } else if ((Board::enpas_sq_ != sq120::NONE) &&
               (Square::GetRank(sqr) == rank::R4)) {
      if ((sqr + 11) == Board::enpas_sq_)
        MoveGen::SetBPawnCEnpas(sqr, sqr + 11);
      else if (Piece::GetColor(Board::board_120_[sqr + 11]) == side::white)
        MoveGen::SetCNormal(sqr, sqr + 11, piece::b_pawn);

      if ((sqr + 9) == Board::enpas_sq_)
        MoveGen::SetBPawnCEnpas(sqr, sqr + 9);
      else if (Piece::GetColor(Board::board_120_[sqr + 9]) == side::white)
        MoveGen::SetCNormal(sqr, sqr + 9, piece::b_pawn);

      if (Board::board_120_[sqr + 10] == piece::empty)
        MoveGen::SetQNormal(sqr, sqr + 10, piece::b_pawn);
    } else if (Square::GetRank(sqr) == rank::R7) {
      if (Board::board_120_[sqr + 10] == piece::empty) {
        MoveGen::SetQNormal(sqr, sqr + 10, piece::b_pawn);

        if (Board::board_120_[sqr + 20] == piece::empty)
          MoveGen::SetBPawnQ2Sq(sqr, sqr + 20);
      }
      if (Piece::GetColor(Board::board_120_[sqr + 11]) == side::white)
        MoveGen::SetCNormal(sqr, sqr + 11, piece::b_pawn);
      if (Piece::GetColor(Board::board_120_[sqr + 9]) == side::white)
        MoveGen::SetCNormal(sqr, sqr + 9, piece::b_pawn);
    } else {
      if (Piece::GetColor(Board::board_120_[sqr + 11]) == side::white)
        MoveGen::SetCNormal(sqr, sqr + 11, piece::b_pawn);
      if (Board::board_120_[sqr + 10] == piece::empty)
        MoveGen::SetQNormal(sqr, sqr + 10, piece::b_pawn);
      if (Piece::GetColor(Board::board_120_[sqr + 9]) == side::white)
        MoveGen::SetCNormal(sqr, sqr + 9, piece::b_pawn);
    }
  }
}
void MoveGen::AllBKnightMoves() {
  for (size_t k = 0, size = Board::piece_list_[piece::b_knight].size();
       k != size; ++k) {
    unsigned int t_sq64 =
        Square::Conv120To64(Board::piece_list_[piece::b_knight][k]);
    for (size_t i = 0, size_i = Square::move_to_sq_for_knight_[t_sq64].size();
         i != size_i; ++i) {
      Piece_t piece = *Square::move_to_sq_for_knight_[t_sq64][i];
      if (piece == piece::empty)
        MoveGen::SetQNormal(Board::piece_list_[piece::b_knight][k],
                            Square::index_to_sq_for_knight_[t_sq64][i],
                            piece::b_knight);
      else if (Piece::GetColor(piece) == side::white)
        MoveGen::SetCNormal(Board::piece_list_[piece::b_knight][k],
                            Square::index_to_sq_for_knight_[t_sq64][i],
                            piece::b_knight);
    }
  }
}
void MoveGen::AllBKingMoves() {
  // for(unsigned int k=0,size=Board::piece_list_[piece::b_king].size();
  // k!=size; ++k) {
  unsigned int t_sq64 =
      Square::Conv120To64(Board::piece_list_[piece::b_king][0]);
  for (size_t i = 0, size_i = Square::move_to_sq_for_king_[t_sq64].size();
       i != size_i; ++i) {
    Piece_t piece = *Square::move_to_sq_for_king_[t_sq64][i];
    if (piece == piece::empty)
      MoveGen::SetQNormal(Board::piece_list_[piece::b_king][0],
                          Square::index_to_sq_for_king_[t_sq64][i],
                          piece::b_king);
    else if (Piece::GetColor(piece) == side::white)
      MoveGen::SetCNormal(Board::piece_list_[piece::b_king][0],
                          Square::index_to_sq_for_king_[t_sq64][i],
                          piece::b_king);
  }
  //}
  if ((Board::castling_perm_ & castling::b_king_side) &&
      (Board::board_120_[sq120::F8] == piece::empty) &&
      (Board::board_120_[sq120::G8] == piece::empty) &&
      (!Square::IsSqAttacked(sq120::E8)) && (!Square::IsSqAttacked(sq120::F8)))
    MoveGen::SetBKingCastl();
  if ((Board::castling_perm_ & castling::b_queen_side) &&
      (Board::board_120_[sq120::B8] == piece::empty) &&
      (Board::board_120_[sq120::C8] == piece::empty) &&
      (Board::board_120_[sq120::D8] == piece::empty) &&
      (!Square::IsSqAttacked(sq120::D8)) && (!Square::IsSqAttacked(sq120::E8)))
    MoveGen::SetBQueenCastl();
}
void MoveGen::AllBBishopMoves() {
  for (size_t k = 0, size = Board::piece_list_[piece::b_bishop].size();
       k != size; ++k) {
    Square_t sq = Board::piece_list_[piece::b_bishop][k];
    for (unsigned int i = 0; i < 4; ++i) {
      int8_t inc = Square::delta_for_bishop_[i];
      Square_t index = sq + inc;
      Piece_t piece = Board::board_120_[index];
      while (piece == piece::empty) {
        MoveGen::SetQNormal(sq, index, piece::b_bishop);
        index += inc;
        piece = Board::board_120_[index];
      }
      if (Piece::GetColor(piece) == side::white)
        MoveGen::SetCNormal(sq, index, piece::b_bishop);
    }
  }
}
void MoveGen::AllBRookMoves() {
  for (size_t k = 0, size = Board::piece_list_[piece::b_rook].size(); k != size;
       ++k) {
    Square_t sq = Board::piece_list_[piece::b_rook][k];
    for (unsigned int i = 0; i < 4; ++i) {
      int8_t inc = Square::delta_for_rook_[i];
      Square_t index = sq + inc;
      Piece_t piece = Board::board_120_[index];
      while (piece == piece::empty) {
        MoveGen::SetQNormal(sq, index, piece::b_rook);
        index += inc;
        piece = Board::board_120_[index];
      }
      if (Piece::GetColor(piece) == side::white)
        MoveGen::SetCNormal(sq, index, piece::b_rook);
    }
  }
}
void MoveGen::AllBQueenMoves() {
  for (size_t k = 0, size = Board::piece_list_[piece::b_queen].size();
       k != size; ++k) {
    Square_t sq = Board::piece_list_[piece::b_queen][k];
    for (unsigned int i = 0; i < 8; ++i) {
      int8_t inc = Square::delta_for_queen_[i];
      Square_t index = sq + inc;
      Piece_t piece = Board::board_120_[index];
      while (piece == piece::empty) {
        MoveGen::SetQNormal(sq, index, piece::b_queen);
        index += inc;
        piece = Board::board_120_[index];
      }
      if (Piece::GetColor(piece) == side::white)
        MoveGen::SetCNormal(sq, index, piece::b_queen);
    }
  }
}
void MoveGen::AllMoves() {
  MoveGen::movelist_.clear();
  MoveGen::movelist_.reserve(150);
  if (Board::side_to_move_ == side::white) {
    MoveGen::AllWPawnMoves();
    MoveGen::AllWKnightMoves();
    MoveGen::AllWKingMoves();
    MoveGen::AllWBishopMoves();
    MoveGen::AllWRookMoves();
    MoveGen::AllWQueenMoves();
  } else {
    MoveGen::AllBPawnMoves();
    MoveGen::AllBKnightMoves();
    MoveGen::AllBKingMoves();
    MoveGen::AllBBishopMoves();
    MoveGen::AllBRookMoves();
    MoveGen::AllBQueenMoves();
  }
  return;
}
void MoveGen::OnlyCapPromMoves() {
  MoveGen::movelist_.clear();
  MoveGen::movelist_.reserve(50);
  if (Board::side_to_move_ == side::white) {
    for (size_t i = 0, size = Board::piece_list_[piece::w_pawn].size();
         i != size; ++i) {
      Square_t sqr = Board::piece_list_[piece::w_pawn][i];
      if (Square::GetRank(sqr) == rank::R7) {
        if (Piece::GetColor(Board::board_120_[sqr - 11]) == side::black)
          MoveGen::SetWPawnCProm(sqr, sqr - 11);
        if (Board::board_120_[sqr - 10] == piece::empty)
          MoveGen::SetWPawnQProm(sqr, sqr - 10);
        if (Piece::GetColor(Board::board_120_[sqr - 9]) == side::black)
          MoveGen::SetWPawnCProm(sqr, sqr - 9);
      } else if ((Board::enpas_sq_ != sq120::NONE) &&
                 (Square::GetRank(sqr) == rank::R5)) {
        if ((sqr - 11) == Board::enpas_sq_)
          MoveGen::SetWPawnCEnpas(sqr, sqr - 11);
        else if (Piece::GetColor(Board::board_120_[sqr - 11]) == side::black)
          MoveGen::SetCNormal(sqr, sqr - 11, piece::w_pawn);

        if ((sqr - 9) == Board::enpas_sq_)
          MoveGen::SetWPawnCEnpas(sqr, sqr - 9);
        else if (Piece::GetColor(Board::board_120_[sqr - 9]) == side::black)
          MoveGen::SetCNormal(sqr, sqr - 9, piece::w_pawn);
      } else {
        if (Piece::GetColor(Board::board_120_[sqr - 11]) == side::black)
          MoveGen::SetCNormal(sqr, sqr - 11, piece::w_pawn);
        if (Piece::GetColor(Board::board_120_[sqr - 9]) == side::black)
          MoveGen::SetCNormal(sqr, sqr - 9, piece::w_pawn);
      }
    }
    for (size_t k = 0, size = Board::piece_list_[piece::w_knight].size();
         k != size; ++k) {
      Square_t t_sq64 =
          Square::Conv120To64(Board::piece_list_[piece::w_knight][k]);
      for (size_t i = 0, size_i = Square::move_to_sq_for_knight_[t_sq64].size();
           i != size_i; ++i) {
        Piece_t t_piece = *Square::move_to_sq_for_knight_[t_sq64][i];
        if (Piece::GetColor(t_piece) == side::black)
          MoveGen::SetCNormal(Board::piece_list_[piece::w_knight][k],
                              Square::index_to_sq_for_knight_[t_sq64][i],
                              piece::w_knight);
      }
    }
    // for(unsigned int k=0,size=Board::piece_list_[piece::w_king].size();
    // k!=size; ++k) {
    unsigned int t_sq64 =
        Square::Conv120To64(Board::piece_list_[piece::w_king][0]);
    for (size_t i = 0, size_i = Square::move_to_sq_for_king_[t_sq64].size();
         i != size_i; ++i) {
      Piece_t piece = *Square::move_to_sq_for_king_[t_sq64][i];
      if (Piece::GetColor(piece) == side::black)
        MoveGen::SetCNormal(Board::piece_list_[piece::w_king][0],
                            Square::index_to_sq_for_king_[t_sq64][i],
                            piece::w_king);
    }
    //}
    for (size_t k = 0, size = Board::piece_list_[piece::w_bishop].size();
         k != size; ++k) {
      Square_t sq = Board::piece_list_[piece::w_bishop][k];
      for (unsigned int i = 0; i < 4; ++i) {
        int8_t inc = Square::delta_for_bishop_[i];
        Square_t index = sq + inc;
        Piece_t piece = Board::board_120_[index];
        while (piece == piece::empty) {
          index += inc;
          piece = Board::board_120_[index];
        }
        if (Piece::GetColor(piece) == side::black) {
          MoveGen::SetCNormal(sq, index, piece::w_bishop);
        }
      }
    }
    for (size_t k = 0, size = Board::piece_list_[piece::w_rook].size();
         k != size; ++k) {
      Square_t sq = Board::piece_list_[piece::w_rook][k];
      for (unsigned int i = 0; i < 4; ++i) {
        int8_t inc = Square::delta_for_rook_[i];
        Square_t index = sq + inc;
        Piece_t piece = Board::board_120_[index];
        while (piece == piece::empty) {
          index += inc;
          piece = Board::board_120_[index];
        }
        if (Piece::GetColor(piece) == side::black) {
          MoveGen::SetCNormal(sq, index, piece::w_rook);
        }
      }
    }

    for (size_t k = 0, size = Board::piece_list_[piece::w_queen].size();
         k != size; ++k) {
      Square_t sq = Board::piece_list_[piece::w_queen][k];
      for (unsigned int i = 0; i < 8; ++i) {
        int8_t inc = Square::delta_for_queen_[i];
        Square_t index = sq + inc;
        Piece_t piece = Board::board_120_[index];
        while (piece == piece::empty) {
          index += inc;
          piece = Board::board_120_[index];
        }
        if (Piece::GetColor(piece) == side::black) {
          MoveGen::SetCNormal(sq, index, piece::w_queen);
        }
      }
    }
  } else {
    for (size_t i = 0, size = Board::piece_list_[piece::b_pawn].size();
         i != size; ++i) {
      Square_t sqr = Board::piece_list_[piece::b_pawn][i];
      if (Square::GetRank(sqr) == rank::R2) {
        if (Piece::GetColor(Board::board_120_[sqr + 9]) == side::white)
          MoveGen::SetBPawnCProm(sqr, sqr + 9);
        if (Board::board_120_[sqr + 10] == piece::empty)
          MoveGen::SetBPawnQProm(sqr, sqr + 10);
        if (Piece::GetColor(Board::board_120_[sqr + 11]) == side::white)
          MoveGen::SetBPawnCProm(sqr, sqr + 11);
      } else if ((Board::enpas_sq_ != sq120::NONE) &&
                 (Square::GetRank(sqr) == rank::R4)) {
        if ((sqr + 9) == Board::enpas_sq_)
          MoveGen::SetBPawnCEnpas(sqr, sqr + 9);
        else if (Piece::GetColor(Board::board_120_[sqr + 9]) == side::white)
          MoveGen::SetCNormal(sqr, sqr + 9, piece::b_pawn);
        if ((sqr + 11) == Board::enpas_sq_)
          MoveGen::SetBPawnCEnpas(sqr, sqr + 11);
        else if (Piece::GetColor(Board::board_120_[sqr + 11]) == side::white)
          MoveGen::SetCNormal(sqr, sqr + 11, piece::b_pawn);
      } else {
        if (Piece::GetColor(Board::board_120_[sqr + 9]) == side::white)
          MoveGen::SetCNormal(sqr, sqr + 9, piece::b_pawn);
        if (Piece::GetColor(Board::board_120_[sqr + 11]) == side::white)
          MoveGen::SetCNormal(sqr, sqr + 11, piece::b_pawn);
      }
    }

    for (size_t k = 0, size = Board::piece_list_[piece::b_knight].size();
         k != size; ++k) {
      unsigned int t_sq64 =
          Square::Conv120To64(Board::piece_list_[piece::b_knight][k]);
      for (size_t i = 0, size_i = Square::move_to_sq_for_knight_[t_sq64].size();
           i != size_i; ++i) {
        Piece_t piece = *Square::move_to_sq_for_knight_[t_sq64][i];
        if (Piece::GetColor(piece) == side::white)
          MoveGen::SetCNormal(Board::piece_list_[piece::b_knight][k],
                              Square::index_to_sq_for_knight_[t_sq64][i],
                              piece::b_knight);
      }
    }

    // for(unsigned int k=0,size=Board::piece_list_[piece::b_king].size();
    // k!=size; ++k) {
    unsigned int t_sq64 =
        Square::Conv120To64(Board::piece_list_[piece::b_king][0]);
    for (size_t i = 0, size_i = Square::move_to_sq_for_king_[t_sq64].size();
         i != size_i; ++i) {
      Piece_t piece = *Square::move_to_sq_for_king_[t_sq64][i];
      if (Piece::GetColor(piece) == side::white)
        MoveGen::SetCNormal(Board::piece_list_[piece::b_king][0],
                            Square::index_to_sq_for_king_[t_sq64][i],
                            piece::b_king);
    }
    //}
    for (size_t k = 0, size = Board::piece_list_[piece::b_bishop].size();
         k != size; ++k) {
      Square_t sq = Board::piece_list_[piece::b_bishop][k];
      for (unsigned int i = 0; i < 4; ++i) {
        int8_t inc = Square::delta_for_bishop_[i];
        Square_t index = sq + inc;
        Piece_t piece = Board::board_120_[index];
        while (piece == piece::empty) {
          index += inc;
          piece = Board::board_120_[index];
        }
        if (Piece::GetColor(piece) == side::white)
          MoveGen::SetCNormal(sq, index, piece::b_bishop);
      }
    }
    for (size_t k = 0, size = Board::piece_list_[piece::b_rook].size();
         k != size; ++k) {
      Square_t sq = Board::piece_list_[piece::b_rook][k];
      for (unsigned int i = 0; i < 4; ++i) {
        int8_t inc = Square::delta_for_rook_[i];
        Square_t index = sq + inc;
        Piece_t piece = Board::board_120_[index];
        while (piece == piece::empty) {
          index += inc;
          piece = Board::board_120_[index];
        }
        if (Piece::GetColor(piece) == side::white)
          MoveGen::SetCNormal(sq, index, piece::b_rook);
      }
    }

    for (size_t k = 0, size = Board::piece_list_[piece::b_queen].size();
         k != size; ++k) {
      Square_t sq = Board::piece_list_[piece::b_queen][k];
      for (unsigned int i = 0; i < 8; ++i) {
        int8_t inc = Square::delta_for_queen_[i];
        Square_t index = sq + inc;
        Piece_t piece = Board::board_120_[index];
        while (piece == piece::empty) {
          index += inc;
          piece = Board::board_120_[index];
        }
        if (Piece::GetColor(piece) == side::white)
          MoveGen::SetCNormal(sq, index, piece::b_queen);
      }
    }
  }
}
void MoveGen::OnlyTargetMoves(Move_t mv) {
  MoveGen::movelist_.clear();
  MoveGen::movelist_.reserve(50);

  switch (Board::board_120_[Move::GetStartSq(mv)]) {
    case piece::w_pawn:
      MoveGen::AllWPawnMoves();
      break;
    case piece::b_pawn:
      MoveGen::AllBPawnMoves();
      break;
    case piece::w_knight:
      MoveGen::AllWKnightMoves();
      break;
    case piece::b_knight:
      MoveGen::AllBKnightMoves();
      break;
    case piece::w_bishop:
      MoveGen::AllWBishopMoves();
      break;
    case piece::b_bishop:
      MoveGen::AllBBishopMoves();
      break;
    case piece::w_rook:
      MoveGen::AllWRookMoves();
      break;
    case piece::b_rook:
      MoveGen::AllBRookMoves();
      break;
    case piece::w_queen:
      MoveGen::AllWQueenMoves();
      break;
    case piece::b_queen:
      MoveGen::AllBQueenMoves();
      break;
    case piece::w_king:
      MoveGen::AllWKingMoves();
      break;
    case piece::b_king:
      MoveGen::AllBKingMoves();
      break;
  }
}

void MoveGen::PrintMoveList() {
  for (unsigned int i = 0; i < MoveGen::movelist_.size(); ++i) {
    std::cout << i << "\t";
    Move::PrintCoordNot(MoveGen::movelist_[i].move_);
    std::cout << "\n";
  }
}

Move_t MoveGen::IsInListByMask(Move_t move_mask, Move_t inp_move) {
  // MoveGen::AllMoves();
  MoveGen::OnlyTargetMoves(inp_move);
  for (unsigned int i = 0; i < MoveGen::movelist_.size(); ++i) {
    if ((MoveGen::movelist_[i].move_ & move_mask) == inp_move)
      return MoveGen::movelist_[i].move_;
  }
  // std::cout << "move can not found!"
  //          << "\n";
  return 0;
}
Move_t MoveGen::IsInList(Move_t inp_move) {
  // MoveGen::AllMoves();
  MoveGen::OnlyTargetMoves(inp_move);
  for (unsigned int i = 0; i < MoveGen::movelist_.size(); ++i) {
    if (MoveGen::movelist_[i].move_ == inp_move) return inp_move;
  }
  // std::cout << "move can not found!"
  //          << "\n";
  return 0;
}
bool MoveGen::IsLegal(Move_t move) {
  if (MakeMove::DoMove(move)) {
    MakeMove::UndoMove();
    return true;
  }
  return false;
}
void MoveGen::Init() {
  for (Piece_t mov = piece::w_pawn; mov <= piece::b_king; ++mov)
    for (Piece_t capt = piece::w_pawn; capt <= piece::b_king; ++capt)
      MoveGen::movcapt_[mov][capt] =
          Piece::GetValue(capt) - (Piece::GetValue(mov) * 0.6) + CAP_SC;
}