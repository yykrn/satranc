#ifndef PLAYER_H_
#define PLAYER_H_

struct Player {
  Bit64_t pawn_bb_ = 0;
  uint32_t material_ = 0;
  int32_t pst_ = 0;
  Square_t king_sq_ = sq120::NONE;
  void Reset() {
    pawn_bb_ = 0;
    material_ = 0;
    pst_ = 0;
    king_sq_ = sq120::NONE;
  }
};

#endif /* PLAYER_H_ */
