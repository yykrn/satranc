#ifndef EVALUATION_H
#define EVALUATION_H

#include "Square.h"

class Evaluation {
 private:
 public:
  static const std::array<int8_t, INTERNAL_BOARD_SIZE> pst_pawn_;
  static const std::array<int8_t, INTERNAL_BOARD_SIZE> pst_knight_;
  static const std::array<int8_t, INTERNAL_BOARD_SIZE> pst_bishop_;
  static const std::array<int8_t, INTERNAL_BOARD_SIZE> pst_rook_;
  static const std::array<int8_t, INTERNAL_BOARD_SIZE> pst_queen_;
  static const std::array<int8_t, INTERNAL_BOARD_SIZE> pst_king_;

  static Square_t IndexBlack(const Square_t sq) {
    return (63 - Square::Conv120To64(sq));
  }
  static int32_t EvalBoard();
  static int32_t GetPstForPiece(const Square_t sq, const Piece_t pc);
};

#endif