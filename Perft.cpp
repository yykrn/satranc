#include "Perft.h"

long Perft::leafNodes{};

void Perft::PerftRec(int depth) {
  if (depth == 0) {
    Perft::leafNodes++;
    return;
  }

  MoveGen::AllMoves();
  std::vector<MoveListEntry> moves = std::move(MoveGen::movelist_);

  int MoveNum = 0;
  for (size_t size = moves.size(); MoveNum != size; ++MoveNum) {
    if (!MakeMove::DoMove(moves[MoveNum].move_)) {
      continue;
    }
    Perft::PerftRec(depth - 1);
    MakeMove::UndoMove();
  }

  return;
}

void Perft::PerftTest(int depth) {
  auto t1 = std::chrono::high_resolution_clock::now();
  std::cout << "Starting Test To Depth:" << depth << "\n";
  Perft::leafNodes = 0;

  MoveGen::AllMoves();
  std::vector<MoveListEntry> moves = std::move(MoveGen::movelist_);

  Move_t move{};
  int MoveNum = 0;
  for (size_t size = moves.size(); MoveNum != size; ++MoveNum) {
    move = moves[MoveNum].move_;
    if (!MakeMove::DoMove(move)) {
      continue;
    }
    long cumnodes = leafNodes;
    Perft::PerftRec(depth - 1);
    MakeMove::UndoMove();
    long oldnodes = leafNodes - cumnodes;
    std::cout << "move " << (MoveNum + 1) << " : ";
    Move::PrintCoordNot(move);
    std::cout << " : " << oldnodes << "\n";
  }
  auto t2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> diff = t2 - t1;
  std::cout << "\nTest Complete :" << leafNodes << " nodes visited"
            << "\n";
  std::cout << "Time :" << diff.count() << " s"
            << "\n";

  return;
}