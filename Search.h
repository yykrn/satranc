#ifndef SEARCH_H
#define SEARCH_H

#include "definitions.h"

struct MoveListEntry;

enum class bound { alpha, exact, beta, none };

struct TranspEntry {
  uint64_t hash_;
  Move_t move_;
  int32_t score_;
  uint8_t depth_;
  bound value_;
};

struct KillerEntry {
  Move_t move1;
  Move_t move2;
};

struct Timer {
  std::chrono::high_resolution_clock::time_point start_;
  std::chrono::high_resolution_clock::time_point stop_;
  bool timesup_;
  bool time_limit_;
  bool min_one_legal;
};

class Search {
 private:
  static uint8_t depth_;
  static uint64_t nodes_;
  static std::vector<Move_t> pv_line_;
  static Timer timer_;

 public:
  static std::vector<TranspEntry> transp_table;
  static std::vector<TranspEntry> transp_table2;
  static std::array<KillerEntry, SEARCH_DEPTH_LIMIT> killer_heu_;
  static std::array<Move_t, 262144> history_heu_;  // 2^18

  static bool GetPVMove(uint8_t depth, TranspEntry &te);
  static void SetPVline(int depth);
  static void ResetData(uint64_t t);
  static void IsTimeUp();
  static int32_t Negamax(int32_t alpha, int32_t beta, uint8_t depth);
  static int32_t Qsearch(int32_t alpha, int32_t beta);
  static void IterativeSearch();

  static Move_t IsInPVTable();

  static void Init(int32_t size = 256);

  static bool AddTranspTable(TranspEntry &te);
  static bool GetTranspTable(uint8_t depth, TranspEntry &te);

  static void ResetTT();
};

#include "MoveGen.h"
#include "Evaluation.h"
#include "Uci.h"

#endif