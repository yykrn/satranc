#include "Search.h"

uint8_t Search::depth_;
uint64_t Search::nodes_;
Timer Search::timer_;

std::vector<TranspEntry> Search::transp_table;
std::vector<TranspEntry> Search::transp_table2;

std::vector<Move_t> Search::pv_line_;

std::array<KillerEntry, SEARCH_DEPTH_LIMIT> Search::killer_heu_{};
std::array<Move_t, 262144> Search::history_heu_{};

Move_t Search::IsInPVTable() {
  TranspEntry te =
      Search::transp_table[Board::hash_key_ % Search::transp_table.size()];
  return te.move_;
}

void Search::SetPVline(int depth) {
  Search::pv_line_.clear();
  Search::pv_line_.reserve(depth);

  TranspEntry te{0, 0, 0, 0, bound::none};
  Move_t mv = 0;

  if (Search::GetPVMove(depth, te)) mv = te.move_;

  while (mv && depth--) {
    if (MoveGen::IsInList(mv)) {
      if (MakeMove::DoMove(mv)) {
        Search::pv_line_.push_back(mv);
        TranspEntry temp{0, 0, 0, 0, bound::none};

        if (Search::GetPVMove(depth, temp))
          mv = temp.move_;
        else
          break;
      } else
        break;
    } else
      break;
  }

  size_t found = Search::pv_line_.size();

  while (found--) MakeMove::UndoMove();
}
void Search::ResetData(uint64_t t) {
  Search::timer_.time_limit_ = false;
  Search::timer_.start_ = std::chrono::high_resolution_clock::now();
  Search::timer_.stop_ =
      Search::timer_.start_ + std::chrono::milliseconds(t);  // sn limit
  if (t != 0) {
    Search::timer_.time_limit_ = true;
  }

  // Search::history_heu_ = {};
  // Search::killer_heu_ = {};

  Search::timer_.timesup_ = false;
  Search::timer_.min_one_legal = false;

  Search::nodes_ = 0;

  MakeMove::search_depth_ = 0;

  Search::depth_ = SEARCH_DEPTH_LIMIT;
}
void Search::IsTimeUp() {
  if (Search::timer_.time_limit_ && Search::timer_.min_one_legal) {
    if (std::chrono::high_resolution_clock::now() >= Search::timer_.stop_)
      Search::timer_.timesup_ = true;
  }
  if (Uci::InputWaiting()) {
    std::string str;
    std::cin >> str;
    if (str == "stop")
      Search::timer_.timesup_ = true;
    else if (str == "isready")
      std::cout << "readyok\n";
  }
}

int32_t Search::Negamax(int32_t alpha, int32_t beta, uint8_t depth) {
  ++Search::nodes_;
  if ((Search::nodes_ & 1023) == 0) Search::IsTimeUp();
  if (MakeMove::search_depth_ &&
      (Board::fifty_move_ >= 100 || Board::Is3Rep()))  // beraberlik
    return 0;

  if (depth == 0) {
    return Search::Qsearch(alpha,
                           beta);  // Buraya quiescence fonksiyonu yazılmalı
  }

  if (MakeMove::search_depth_ >= SEARCH_DEPTH_LIMIT)
    return Evaluation::EvalBoard();

  int32_t t_alpha = alpha;
  MoveListEntry best_move{0, 0};
  TranspEntry te{0, 0, 0, 0, bound::none};

  if (Search::GetTranspTable(depth, te)) {
    if (te.value_ == bound::alpha) {
      if (te.score_ <= alpha) return alpha;
    } else if (te.value_ == bound::beta) {
      if (te.score_ >= beta) return beta;
    } else if (te.value_ == bound::exact) {
      if (MoveGen::IsInList(te.move_)) {
        if (MakeMove::DoMove(te.move_)) {
          if (!Board::Is3Rep() && Board::fifty_move_ < 100) {
            MakeMove::UndoMove();
            return te.score_;
          } else {
            MakeMove::UndoMove();
            te.move_ = 0;
          }
        } else {
          te.move_ = 0;
        }
      } else {
        te.move_ = 0;
      }
    }
  }
  MoveGen::AllMoves();
  std::vector<MoveListEntry> moves = std::move(MoveGen::movelist_);

  bool no_legal_mv = true;
  bool pvs = false;

  for (size_t num_moves = 0, size = moves.size(); num_moves != size;
       ++num_moves) {
    MoveListEntry *max_sc = &moves[num_moves];
    Move_t mv;
    if (MakeMove::search_depth_ < Search::pv_line_.size())
      mv = Search::pv_line_[MakeMove::search_depth_];
    else
      mv = te.move_;

    if (mv != 0) {
      for (unsigned int i = num_moves + 1; i != size; ++i) {
        if (max_sc->move_ == mv) {
          max_sc->score_ = PV_SC;
          break;
        } else if (max_sc->score_ < moves[i].score_)
          max_sc = &moves[i];
      }
    } else {
      for (unsigned int i = num_moves + 1; i != size; ++i) {
        if (max_sc->score_ < moves[i].score_) max_sc = &moves[i];
      }
    }

    MoveListEntry tmp = moves[num_moves];
    moves[num_moves] = *max_sc;
    *max_sc = tmp;

    tmp = moves[num_moves];

    if (!MakeMove::DoMove(tmp.move_)) {
      continue;
    }
    no_legal_mv = false;

    int32_t value;

    if (pvs) {
      value = -Search::Negamax(-alpha - 1, -alpha, depth - 1);
      if (value > alpha && value < beta)
        value = -Search::Negamax(-beta, -value, depth - 1);
    } else {
      value = -Search::Negamax(-beta, -alpha, depth - 1);
    }

    MakeMove::UndoMove();

    if (Search::timer_.timesup_) return 0;

    if (value >= beta) {
      if (!Move::IsCaptureMove(tmp.move_)) {
        Search::history_heu_[tmp.move_ & 0x0003ffff] += (depth ^ 2);
        Search::killer_heu_[MakeMove::search_depth_].move2 =
            Search::killer_heu_[MakeMove::search_depth_].move1;
        Search::killer_heu_[MakeMove::search_depth_].move1 = tmp.move_;
      }
      TranspEntry ttentry{Board::hash_key_, tmp.move_, beta, depth,
                          bound::beta};
      Search::AddTranspTable(ttentry);
      return beta;
    }

    if (value > alpha) {
      alpha = value;
      best_move = tmp;
      pvs = true;
    }
  }

  if (no_legal_mv) {
    if (Square::IsSqAttacked(Board::player_[Board::side_to_move_].king_sq_))
      return -MATE_SCORE + MakeMove::search_depth_;
    else  // pat
      return 0;
  }
  if (alpha > t_alpha) {
    TranspEntry ttentry{Board::hash_key_, best_move.move_, alpha, depth,
                        bound::exact};
    Search::AddTranspTable(ttentry);

    if (!Search::timer_.min_one_legal) Search::timer_.min_one_legal = true;

  } else if (alpha == t_alpha) {
    TranspEntry ttentry{Board::hash_key_, 0, alpha, depth, bound::alpha};
    Search::AddTranspTable(ttentry);
  }

  return alpha;
}
void Search::IterativeSearch() {
  // Search::ResetData();

  Search::pv_line_.clear();
  int32_t win_width = 100;
  int32_t alpha = -MATE_SCORE;
  int32_t beta = MATE_SCORE;

  for (unsigned int i = 1; i <= Search::depth_; ++i) {
    int32_t max_value = Search::Negamax(alpha, beta, i);
    if (max_value >= beta || max_value <= alpha)
      max_value = Search::Negamax(-MATE_SCORE, MATE_SCORE, i);

    alpha = max_value - win_width;
    beta = max_value + win_width;

    // info depth 2 score cp 214 time 1242 nodes 2124 nps 34928 pv e2e4 e7e5
    // g1f3
    if (Search::timer_.timesup_) break;

    Search::SetPVline(i);

    long long ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(
                            std::chrono::high_resolution_clock::now() -
                            Search::timer_.start_).count();

    std::cout << "info score cp " << max_value << " depth " << i;
    std::cout << " nodes " << Search::nodes_;
    //<< " nps "<< (Search::nodes_ * 1000) / ms_time;
    std::cout << " time " << ms_time;
    std::cout << " pv ";

    for (auto &m : Search::pv_line_) {
      Move::PrintCoordNot(m);
    }

    std::cout << "\n";

    if ((std::abs(max_value) + i) > MATE_SCORE) break;
  }

  std::cout << "bestmove ";
  Move::PrintCoordNot(Search::pv_line_[0]);
  std::cout << "\n";
}
int32_t Search::Qsearch(int32_t alpha, int32_t beta) {
  ++Search::nodes_;
  if ((Search::nodes_ & 1023) == 0) Search::IsTimeUp();
  if (MakeMove::search_depth_ >= SEARCH_DEPTH_LIMIT)
    return Evaluation::EvalBoard();

  int32_t pos_val = Evaluation::EvalBoard();

  if (pos_val >= beta) return beta;
  if (pos_val > alpha) alpha = pos_val;

  int32_t t_alpha = alpha;
  TranspEntry best_move{};

  MoveGen::OnlyCapPromMoves();
  std::vector<MoveListEntry> moves = std::move(MoveGen::movelist_);
  bool no_legal_mv = true;
  for (size_t num_moves = 0, size = moves.size(); num_moves != size;
       ++num_moves) {
    MoveListEntry *max_sc = &moves[num_moves];
    Search::GetTranspTable(0, best_move);
    Move_t mv = best_move.move_;
    if (max_sc->move_ != mv) {
      if ((mv != 0) && (Move::IsCaptureMove(mv) || Move::IsPromotionMove(mv))) {
        for (unsigned int i = num_moves + 1; i != size; ++i) {
          if (max_sc->move_ == mv) {
            max_sc->score_ = PV_SC;
            break;
          } else if (max_sc->score_ < moves[i].score_)
            max_sc = &moves[i];
        }
      } else {
        for (unsigned int i = num_moves + 1; i != size; ++i) {
          if (max_sc->score_ < moves[i].score_) max_sc = &moves[i];
        }
      }
    } else {
      max_sc->score_ = PV_SC;
    }

    MoveListEntry tmp = moves[num_moves];
    moves[num_moves] = *max_sc;
    *max_sc = tmp;

    tmp = moves[num_moves];

    if (!MakeMove::DoMove(tmp.move_)) {
      continue;
    }

    int32_t value = -Search::Qsearch(-beta, -alpha);

    MakeMove::UndoMove();
    if (Search::timer_.timesup_) return value;

    if (value >= beta) return beta;

    if (value > alpha) {
      alpha = value;
    }
  }
  return alpha;
}
void Search::Init(int32_t size) {
  Search::transp_table.resize((size * MB_IN_BYTES / 2) / sizeof(TranspEntry),
                              TranspEntry{0, 0, 0, 0, bound::none});
  Search::transp_table2.resize((size * MB_IN_BYTES / 2) / sizeof(TranspEntry),
                               TranspEntry{0, 0, 0, 0, bound::none});
}
bool Search::AddTranspTable(TranspEntry &te) {
  if (te.score_ < (-MATE_SCORE + SEARCH_DEPTH_LIMIT))
    te.score_ -= MakeMove::search_depth_;
  else if (te.score_ > (MATE_SCORE - SEARCH_DEPTH_LIMIT))
    te.score_ += MakeMove::search_depth_;
  size_t index = te.hash_ % Search::transp_table.size();
  if (te.depth_ > Search::transp_table[index].depth_) {
    auto temp = Search::transp_table[index];
    Search::transp_table[index] = te;
    Search::transp_table2[index] = temp;
    return true;
  } else {
    Search::transp_table2[index] = te;
    return false;
  }
}
bool Search::GetTranspTable(uint8_t depth, TranspEntry &te) {
  size_t index = Board::hash_key_ % Search::transp_table.size();
  if (Search::transp_table[index].hash_ == Board::hash_key_) {
    te = Search::transp_table[index];
    if (te.score_ < (-MATE_SCORE + SEARCH_DEPTH_LIMIT))
      te.score_ += MakeMove::search_depth_;
    else if (te.score_ > (MATE_SCORE - SEARCH_DEPTH_LIMIT))
      te.score_ -= MakeMove::search_depth_;
    if (Search::transp_table[index].depth_ >= depth) return true;
  } else if (Search::transp_table2[index].hash_ == Board::hash_key_) {
    te = Search::transp_table2[index];
    if (te.score_ < (-MATE_SCORE + SEARCH_DEPTH_LIMIT))
      te.score_ += MakeMove::search_depth_;
    else if (te.score_ > (MATE_SCORE - SEARCH_DEPTH_LIMIT))
      te.score_ -= MakeMove::search_depth_;
    if (Search::transp_table2[index].depth_ >= depth) return true;
  }
  return false;
}
void Search::ResetTT() {
  std::fill(Search::transp_table.begin(), Search::transp_table.end(),
            TranspEntry{0, 0, 0, 0, bound::none});
  std::fill(Search::transp_table2.begin(), Search::transp_table2.end(),
            TranspEntry{0, 0, 0, 0, bound::none});
}
bool Search::GetPVMove(uint8_t depth, TranspEntry &te) {
  size_t index = Board::hash_key_ % Search::transp_table.size();
  if (Search::transp_table[index].hash_ == Board::hash_key_ &&
      Search::transp_table[index].value_ == bound::exact) {
    te = Search::transp_table[index];
    if (Search::transp_table[index].depth_ >= depth) return true;
  } else if (Search::transp_table2[index].hash_ == Board::hash_key_ &&
             Search::transp_table2[index].value_ == bound::exact) {
    te = Search::transp_table2[index];
    if (Search::transp_table2[index].depth_ >= depth) return true;
  }
  return false;
}