#ifndef HISTORY_H
#define HISTORY_H

#include "definitions.h"

struct History {
  uint64_t pos_key_;
  uint32_t move_;
  uint8_t castle_perm_;
  Square_t enpas_sq_;
  uint8_t fifty_move_;
  History() {}
  History(uint64_t poskey, uint32_t move, uint8_t castling, Square_t enpas,
          uint8_t fif)
      : pos_key_(poskey),
        move_(move),
        castle_perm_(castling),
        enpas_sq_(enpas),
        fifty_move_(fif) {}
};

#endif  // !HISTORY_H
