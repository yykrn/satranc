#include "Board.h"

std::array<Piece_t, BOARD_SIZE> Board::board_120_{};

const std::array<Square_t, (BOARD_SIZE - INTERNAL_BOARD_SIZE)>
    Board::offboard_index_{
        0,   1,   2,   3,   4,   5,   6,   7,   8,   9,   10,  11,  12,  13,
        14,  15,  16,  17,  18,  19,  20,  29,  30,  39,  40,  49,  50,  59,
        60,  69,  70,  79,  80,  89,  90,  99,  100, 101, 102, 103, 104, 105,
        106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119};

uint8_t Board::side_to_move_{};
Square_t Board::enpas_sq_{};
uint8_t Board::fifty_move_{};
uint8_t Board::castling_perm_{};
uint64_t Board::hash_key_{};

Bit64_t Board::all_pawn_bb_{};

std::vector<History> Board::history_;
std::array<Piece_t *const, INTERNAL_BOARD_SIZE> Board::board_64_{
    &Board::board_120_[21], &Board::board_120_[22], &Board::board_120_[23],
    &Board::board_120_[24], &Board::board_120_[25], &Board::board_120_[26],
    &Board::board_120_[27], &Board::board_120_[28], &Board::board_120_[31],
    &Board::board_120_[32], &Board::board_120_[33], &Board::board_120_[34],
    &Board::board_120_[35], &Board::board_120_[36], &Board::board_120_[37],
    &Board::board_120_[38], &Board::board_120_[41], &Board::board_120_[42],
    &Board::board_120_[43], &Board::board_120_[44], &Board::board_120_[45],
    &Board::board_120_[46], &Board::board_120_[47], &Board::board_120_[48],
    &Board::board_120_[51], &Board::board_120_[52], &Board::board_120_[53],
    &Board::board_120_[54], &Board::board_120_[55], &Board::board_120_[56],
    &Board::board_120_[57], &Board::board_120_[58], &Board::board_120_[61],
    &Board::board_120_[62], &Board::board_120_[63], &Board::board_120_[64],
    &Board::board_120_[65], &Board::board_120_[66], &Board::board_120_[67],
    &Board::board_120_[68], &Board::board_120_[71], &Board::board_120_[72],
    &Board::board_120_[73], &Board::board_120_[74], &Board::board_120_[75],
    &Board::board_120_[76], &Board::board_120_[77], &Board::board_120_[78],
    &Board::board_120_[81], &Board::board_120_[82], &Board::board_120_[83],
    &Board::board_120_[84], &Board::board_120_[85], &Board::board_120_[86],
    &Board::board_120_[87], &Board::board_120_[88], &Board::board_120_[91],
    &Board::board_120_[92], &Board::board_120_[93], &Board::board_120_[94],
    &Board::board_120_[95], &Board::board_120_[96], &Board::board_120_[97],
    &Board::board_120_[98]};

std::array<std::vector<Square_t>, NUM_PIECE_TYPE> Board::piece_list_{};

std::array<Player, 2> Board::player_{};

void Board::ResetBoard() {
  for (unsigned int i = 0; i < Board::offboard_index_.size(); ++i)
    Board::board_120_[Board::offboard_index_[i]] = sq120::OUT;

  for (unsigned int i = 0; i < Board::board_64_.size(); ++i)
    *Board::board_64_[i] = piece::empty;

  Board::castling_perm_ = castling::NONE;
  Board::enpas_sq_ = sq120::NONE;
  Board::fifty_move_ = 0;
  Board::hash_key_ = 0;
  Board::side_to_move_ = side::NONE;

  Board::all_pawn_bb_.reset();

  Board::history_.clear();
  Board::history_.reserve(300);

  for (unsigned int i = 0; i < Board::piece_list_.size(); ++i) {
    Board::piece_list_[i].clear();
    Board::piece_list_[i].reserve(10);
  }

  Board::player_[0].Reset();
  Board::player_[1].Reset();
}

void Board::PrintBoard() {
  static const std::unordered_map<Square_t, std::string> str_enpas{
      {sq120::A6, "a6"},
      {sq120::B6, "b6"},
      {sq120::C6, "c6"},
      {sq120::D6, "d6"},
      {sq120::E6, "e6"},
      {sq120::F6, "f6"},
      {sq120::G6, "g6"},
      {sq120::H6, "h6"},
      {sq120::A3, "a3"},
      {sq120::B3, "b3"},
      {sq120::C3, "c3"},
      {sq120::D3, "d3"},
      {sq120::E3, "e3"},
      {sq120::F3, "f3"},
      {sq120::G3, "g3"},
      {sq120::H3, "h3"},
      {sq120::NONE, "-"}};
  static const std::array<std::string, 16> str_castle_perm{
      "-", "q",  "k",  "kq",  "Q",  "Qq",  "Qk",  "Qkq",
      "K", "Kq", "Kk", "Kkq", "KQ", "KQq", "KQk", "KQkq"};

  for (unsigned int j = 0; j <= INTERNAL_BOARD_SIZE - 8; j += 8) {
    std::cout << "\n"
              << "\n";
    std::cout << 8 - (j / 8) << "|";
    std::cout << std::setw(4) << to_str::pieces[static_cast<int>(*board_64_[j])]
              << " ";
    std::cout << std::setw(4)
              << to_str::pieces[static_cast<int>(*board_64_[j + 1])] << " ";
    std::cout << std::setw(4)
              << to_str::pieces[static_cast<int>(*board_64_[j + 2])] << " ";
    std::cout << std::setw(4)
              << to_str::pieces[static_cast<int>(*board_64_[j + 3])] << " ";
    std::cout << std::setw(4)
              << to_str::pieces[static_cast<int>(*board_64_[j + 4])] << " ";
    std::cout << std::setw(4)
              << to_str::pieces[static_cast<int>(*board_64_[j + 5])] << " ";
    std::cout << std::setw(4)
              << to_str::pieces[static_cast<int>(*board_64_[j + 6])] << " ";
    std::cout << std::setw(4)
              << to_str::pieces[static_cast<int>(*board_64_[j + 7])] << " ";
  }
  std::cout << "\n"
            << "\n";
  std::cout << "    ---  ---  ---  ---  ---  ---  ---  --- "
            << "\n";
  std::cout << "     a    b    c    d    e    f    g    h "
            << "\n";

  std::cout << "side:" << to_str::str_sides[Board::side_to_move_] << "\n";
  std::cout << "en passant square:" << str_enpas.at(Board::enpas_sq_) << "\n";
  std::cout << "castle permission:" << str_castle_perm[Board::castling_perm_]
            << "\n";
  std::cout << "ply:" << Board::history_.size() << "\n";
  std::cout << "fifty move:" << static_cast<int>(Board::fifty_move_) << "\n";
  std::cout << "full move:" << Board::history_.size() / 2 + 1 << "\n";
  std::cout << "hash key for board:" << Board::hash_key_ << std::endl;
}
void Board::InitMaterial() {
  for (unsigned int i = 0; i < Board::board_64_.size(); ++i) {
    Piece_t piece_t = *Board::board_64_[i];
    if (piece_t != piece::empty) {
      ASSERT(Test::ForPiece(piece_t));
      Board::piece_list_[piece_t].push_back(Square::Conv64To120(i));
    }
  }

  // Board::player_[side::white].material_=0
  for (Piece_t i = piece::w_pawn; i <= piece::w_king; i += 2) {
    Board::player_[side::white].material_ +=
        Piece::GetValue(i) * Board::piece_list_[i].size();
    ASSERT(Board::piece_list_[i].size() <= 10);
  }

  // Board::player_[side::black].material_=0
  for (Piece_t i = piece::b_pawn; i <= piece::b_king; i += 2) {
    Board::player_[side::black].material_ +=
        Piece::GetValue(i) * Board::piece_list_[i].size();
    ASSERT(Board::piece_list_[i].size() <= 10);
  }
  int32_t pst_value_w = 0;
  int32_t pst_value_b = 0;

  for (Square_t sq : Board::piece_list_[piece::w_pawn])
    pst_value_w += (Evaluation::pst_pawn_[Square::Conv120To64(sq)]);

  for (Square_t sq : Board::piece_list_[piece::b_pawn])
    pst_value_b += (Evaluation::pst_pawn_[Evaluation::IndexBlack(sq)]);

  for (Square_t sq : Board::piece_list_[piece::w_knight])
    pst_value_w += (Evaluation::pst_knight_[Square::Conv120To64(sq)]);

  for (Square_t sq : Board::piece_list_[piece::b_knight])
    pst_value_b += (Evaluation::pst_knight_[Evaluation::IndexBlack(sq)]);

  for (Square_t sq : Board::piece_list_[piece::w_bishop])
    pst_value_w += (Evaluation::pst_bishop_[Square::Conv120To64(sq)]);

  for (Square_t sq : Board::piece_list_[piece::b_bishop])
    pst_value_b += (Evaluation::pst_bishop_[Evaluation::IndexBlack(sq)]);

  for (Square_t sq : Board::piece_list_[piece::w_rook])
    pst_value_w += (Evaluation::pst_rook_[Square::Conv120To64(sq)]);

  for (Square_t sq : Board::piece_list_[piece::b_rook])
    pst_value_b += (Evaluation::pst_rook_[Evaluation::IndexBlack(sq)]);

  for (Square_t sq : Board::piece_list_[piece::w_queen])
    pst_value_w += (Evaluation::pst_queen_[Square::Conv120To64(sq)]);

  for (Square_t sq : Board::piece_list_[piece::b_queen])
    pst_value_b += (Evaluation::pst_queen_[Evaluation::IndexBlack(sq)]);

  Board::player_[side::white].pst_ = pst_value_w;
  Board::player_[side::black].pst_ = pst_value_b;

  if (Board::piece_list_[piece::w_king].size())  //
    Board::player_[side::white].king_sq_ = Board::piece_list_[piece::w_king][0];
  if (Board::piece_list_[piece::b_king].size())  //
    Board::player_[side::black].king_sq_ = Board::piece_list_[piece::b_king][0];

  for (unsigned int i = 0; i < Board::piece_list_[piece::w_pawn].size(); ++i) {
    Board::player_[side::white].pawn_bb_.set(
        Square::Conv120To64(Board::piece_list_[piece::w_pawn][i]));
  }

  for (unsigned int i = 0; i < Board::piece_list_[piece::b_pawn].size(); ++i) {
    Board::player_[side::black].pawn_bb_.set(
        Square::Conv120To64(Board::piece_list_[piece::b_pawn][i]));
  }

  Board::all_pawn_bb_ = Board::player_[side::white].pawn_bb_ |
                        Board::player_[side::black].pawn_bb_;
}

bool Board::CalcHashKey() {
  Board::hash_key_ = 0;

  for (unsigned int i = 1; i < NUM_PIECE_TYPE; ++i) {
    for (unsigned int j = 0; j < piece_list_[i].size(); ++j) {
      ASSERT(Test::ForSquare120(piece_list_[i][j]));
      Board::hash_key_ ^= Hash::sqxpiece(piece_list_[i][j]);
    }
  }

  if (Board::side_to_move_ == side::white)
    Board::hash_key_ ^= Hash::white_side();

  ASSERT(Test::ForSide(Board::side_to_move_));

  if (Board::enpas_sq_ != sq120::NONE) {
    ASSERT(Test::ForEnpas(Board::enpas_sq_));
    ASSERT(Board::board_120_[Board::enpas_sq_] == piece::empty);
    Board::hash_key_ ^= Hash::sqxpiece(Board::enpas_sq_);
  }

  Board::hash_key_ ^= Hash::castle_perm(Board::castling_perm_);
  return true;
}

bool Board::ControlData() {
  for (unsigned int i = 0; i < Board::piece_list_.size(); ++i) {
    ASSERT(Board::piece_list_[i].size() == Board::piece_list_[i].size());
    for (unsigned int j = 0; j < Board::piece_list_[i].size(); ++j) {
      ASSERT(Board::board_120_[Board::piece_list_[i][j]] == i);
    }
  }

  ASSERT((Board::all_pawn_bb_.count()) ==
         (Board::piece_list_[piece::w_pawn].size() +
          Board::piece_list_[piece::b_pawn].size()));

  size_t material_w = 0;
  for (unsigned int i = piece::w_pawn; i <= piece::w_king; i += 2)
    material_w += Board::piece_list_[i].size() * Piece::GetValue(i);

  ASSERT(material_w == Board::player_[side::white].material_);

  size_t material_b = 0;
  for (unsigned int i = piece::b_pawn; i <= piece::b_king; i += 2)
    material_b += Board::piece_list_[i].size() * Piece::GetValue(i);

  ASSERT(material_b == Board::player_[side::black].material_);

  Bit64_t white_pawn_bb = Board::player_[side::white].pawn_bb_;
  Bit64_t black_pawn_bb = Board::player_[side::black].pawn_bb_;
  while (white_pawn_bb.any()) {
    ASSERT(Board::board_120_[Square::Conv64To120(
        Bitset::PopMostSign1Bit(white_pawn_bb))]);
  }
  while (black_pawn_bb.any()) {
    ASSERT(Board::board_120_[Square::Conv64To120(
        Bitset::PopMostSign1Bit(black_pawn_bb))]);
  }
  ASSERT((Board::player_[side::white].pawn_bb_ |
          Board::player_[side::black].pawn_bb_) == Board::all_pawn_bb_);

  if (Board::piece_list_[piece::w_king].size())
    ASSERT(Board::board_120_[Board::player_[side::white].king_sq_] ==
           piece::w_king);
  if (Board::piece_list_[piece::b_king].size())
    ASSERT(Board::board_120_[Board::player_[side::black].king_sq_] ==
           piece::b_king);

  ASSERT(Board::side_to_move_ == side::white ||
         Board::side_to_move_ == side::black);

  ASSERT(Board::enpas_sq_ == sq120::NONE ||
         (Square::GetRank(Board::enpas_sq_) == rank::R6 &&
          Board::side_to_move_ == side::white) ||
         (Square::GetRank(Board::enpas_sq_) == rank::R3 &&
          Board::side_to_move_ == side::black));

  uint64_t old_hash_key = Board::hash_key_;
  Board::CalcHashKey();
  ASSERT(old_hash_key == Board::hash_key_);

  return true;
}

bool Board::IsSamePos() {
  ASSERT(Board::history_.size() >= Board::fifty_move_)
  for (size_t k = Board::history_.size() - Board::fifty_move_;
       k < Board::history_.size(); ++k)
    if (Board::hash_key_ == Board::history_[k].pos_key_) return true;
  return false;
}

bool Board::Is3Rep() {
  ASSERT(Board::history_.size() >= Board::fifty_move_);
  unsigned int j = 0;
  for (size_t k = Board::history_.size() - Board::fifty_move_;
       k < Board::history_.size(); ++k)
    if (Board::hash_key_ == Board::history_[k].pos_key_) ++j;
  return (j >= 3);
}