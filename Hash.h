#ifndef HASH_H_
#define HASH_H_

#include "Square.h"

class Hash {
 private:
  static const std::array<uint64_t, (NUM_PIECE_TYPE * INTERNAL_BOARD_SIZE)>
      hash_for_sq_piece_;
  static const uint64_t hash_for_white_side_;
  static const std::array<uint64_t, 16> hash_for_cast_perm_;

 public:
  static uint64_t sqxpiece(const Square_t sq) {
    return hash_for_sq_piece_[Square::Conv120To64(sq) +
                              Board::board_120_[sq] * INTERNAL_BOARD_SIZE];
  }

  static uint64_t white_side() { return hash_for_white_side_; }

  static uint64_t castle_perm(const unsigned int i) {
    return hash_for_cast_perm_[i];
  }
};

#endif /* HASH_H_ */
