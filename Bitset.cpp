#include "Bitset.h"

// bitScanReverse
// Kim Walisch, Mark Dickinson

Square_t Bitset::LeftMost1Bit(const Bit64_t &bitmap_t) {
  const static std::array<Square_t, INTERNAL_BOARD_SIZE> index64{
      0,  47, 1,  56, 48, 27, 2,  60, 57, 49, 41, 37, 28, 16, 3,  61,
      54, 58, 35, 52, 50, 42, 21, 44, 38, 32, 29, 23, 17, 11, 4,  62,
      46, 55, 26, 59, 40, 36, 15, 53, 34, 51, 20, 43, 31, 22, 10, 45,
      25, 39, 14, 33, 19, 30, 9,  24, 13, 18, 8,  12, 7,  6,  5,  63};

  const static uint64_t debruijn64{0x03f79d71b4cb0a89};
  uint64_t bitmap{bitmap_t.to_ullong()};
  ASSERT(bitmap != 0);
  bitmap |= (bitmap >> 1);
  bitmap |= bitmap >> 2;
  bitmap |= bitmap >> 4;
  bitmap |= bitmap >> 8;
  bitmap |= bitmap >> 16;
  bitmap |= bitmap >> 32;
  return index64[(bitmap * debruijn64) >> 58];
}

Square_t Bitset::PopMostSign1Bit(Bit64_t &bitmap) {
  ASSERT(bitmap.to_ullong());
  Square_t index{Bitset::LeftMost1Bit(bitmap)};
  ASSERT(Test::ForSquare64(index));
  ASSERT(bitmap[index]);
  bitmap.reset(index);
  return index;
}

void Bitset::DisplayBitset(const Bit64_t &bitmap) {
  for (unsigned int j = 0; j <= INTERNAL_BOARD_SIZE - 8; j += 8) {
    std::cout << "\n"
              << "\n";
    std::cout << 8 - (j / 8) << "|";
    std::cout << std::setw(4) << bitmap[j] << " ";
    std::cout << std::setw(4) << bitmap[j + 1] << " ";
    std::cout << std::setw(4) << bitmap[j + 2] << " ";
    std::cout << std::setw(4) << bitmap[j + 3] << " ";
    std::cout << std::setw(4) << bitmap[j + 4] << " ";
    std::cout << std::setw(4) << bitmap[j + 5] << " ";
    std::cout << std::setw(4) << bitmap[j + 6] << " ";
    std::cout << std::setw(4) << bitmap[j + 7] << " ";
  }
  std::cout << "\n"
            << "\n";
  std::cout << "    ---  ---  ---  ---  ---  ---  ---  --- " << std::endl;
  std::cout << "     a    b    c    d    e    f    g    h " << std::endl;
}