##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=satranc
ConfigurationName      :=Debug
WorkspacePath          := "/home/resul/.codelite/resul"
ProjectPath            := "/home/resul/bitbucket-satranc/satranc"
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=resul
Date                   :=04/04/15
CodeLitePath           :="/home/resul/.codelite"
LinkerName             :=clang++
SharedObjectLinkerName :=clang++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=
PreprocessSuffix       :=.o.i
DebugSwitch            :=-gstab
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="satranc.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  -pg
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := clang++
CC       := clang
CXXFLAGS :=  -pg -g -Wall -std=c++11 -O0 $(Preprocessors)
CFLAGS   :=  -g -Wall -O0 $(Preprocessors)
ASFLAGS  := 
AS       := llvm-as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/bitbucket-satranc_Board.cpp$(ObjectSuffix) $(IntermediateDirectory)/bitbucket-satranc_Fen.cpp$(ObjectSuffix) $(IntermediateDirectory)/bitbucket-satranc_Hash.cpp$(ObjectSuffix) $(IntermediateDirectory)/bitbucket-satranc_main.cpp$(ObjectSuffix) $(IntermediateDirectory)/bitbucket-satranc_Move.cpp$(ObjectSuffix) $(IntermediateDirectory)/bitbucket-satranc_Square.cpp$(ObjectSuffix) $(IntermediateDirectory)/bitbucket-satranc_Piece.cpp$(ObjectSuffix) $(IntermediateDirectory)/bitbucket-satranc_Bitset.cpp$(ObjectSuffix) $(IntermediateDirectory)/bitbucket-satranc_MoveGen.cpp$(ObjectSuffix) $(IntermediateDirectory)/bitbucket-satranc_MakeMove.cpp$(ObjectSuffix) \
	$(IntermediateDirectory)/bitbucket-satranc_Perft.cpp$(ObjectSuffix) $(IntermediateDirectory)/bitbucket-satranc_Search.cpp$(ObjectSuffix) $(IntermediateDirectory)/bitbucket-satranc_Evaluation.cpp$(ObjectSuffix) $(IntermediateDirectory)/bitbucket-satranc_Uci.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/bitbucket-satranc_Board.cpp$(ObjectSuffix): ../Board.cpp 
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/resul/bitbucket-satranc/Board.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/bitbucket-satranc_Board.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/bitbucket-satranc_Board.cpp$(PreprocessSuffix): ../Board.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/bitbucket-satranc_Board.cpp$(PreprocessSuffix) "../Board.cpp"

$(IntermediateDirectory)/bitbucket-satranc_Fen.cpp$(ObjectSuffix): ../Fen.cpp 
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/resul/bitbucket-satranc/Fen.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/bitbucket-satranc_Fen.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/bitbucket-satranc_Fen.cpp$(PreprocessSuffix): ../Fen.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/bitbucket-satranc_Fen.cpp$(PreprocessSuffix) "../Fen.cpp"

$(IntermediateDirectory)/bitbucket-satranc_Hash.cpp$(ObjectSuffix): ../Hash.cpp 
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/resul/bitbucket-satranc/Hash.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/bitbucket-satranc_Hash.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/bitbucket-satranc_Hash.cpp$(PreprocessSuffix): ../Hash.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/bitbucket-satranc_Hash.cpp$(PreprocessSuffix) "../Hash.cpp"

$(IntermediateDirectory)/bitbucket-satranc_main.cpp$(ObjectSuffix): ../main.cpp 
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/resul/bitbucket-satranc/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/bitbucket-satranc_main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/bitbucket-satranc_main.cpp$(PreprocessSuffix): ../main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/bitbucket-satranc_main.cpp$(PreprocessSuffix) "../main.cpp"

$(IntermediateDirectory)/bitbucket-satranc_Move.cpp$(ObjectSuffix): ../Move.cpp 
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/resul/bitbucket-satranc/Move.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/bitbucket-satranc_Move.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/bitbucket-satranc_Move.cpp$(PreprocessSuffix): ../Move.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/bitbucket-satranc_Move.cpp$(PreprocessSuffix) "../Move.cpp"

$(IntermediateDirectory)/bitbucket-satranc_Square.cpp$(ObjectSuffix): ../Square.cpp 
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/resul/bitbucket-satranc/Square.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/bitbucket-satranc_Square.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/bitbucket-satranc_Square.cpp$(PreprocessSuffix): ../Square.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/bitbucket-satranc_Square.cpp$(PreprocessSuffix) "../Square.cpp"

$(IntermediateDirectory)/bitbucket-satranc_Piece.cpp$(ObjectSuffix): ../Piece.cpp 
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/resul/bitbucket-satranc/Piece.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/bitbucket-satranc_Piece.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/bitbucket-satranc_Piece.cpp$(PreprocessSuffix): ../Piece.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/bitbucket-satranc_Piece.cpp$(PreprocessSuffix) "../Piece.cpp"

$(IntermediateDirectory)/bitbucket-satranc_Bitset.cpp$(ObjectSuffix): ../Bitset.cpp 
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/resul/bitbucket-satranc/Bitset.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/bitbucket-satranc_Bitset.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/bitbucket-satranc_Bitset.cpp$(PreprocessSuffix): ../Bitset.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/bitbucket-satranc_Bitset.cpp$(PreprocessSuffix) "../Bitset.cpp"

$(IntermediateDirectory)/bitbucket-satranc_MoveGen.cpp$(ObjectSuffix): ../MoveGen.cpp 
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/resul/bitbucket-satranc/MoveGen.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/bitbucket-satranc_MoveGen.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/bitbucket-satranc_MoveGen.cpp$(PreprocessSuffix): ../MoveGen.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/bitbucket-satranc_MoveGen.cpp$(PreprocessSuffix) "../MoveGen.cpp"

$(IntermediateDirectory)/bitbucket-satranc_MakeMove.cpp$(ObjectSuffix): ../MakeMove.cpp 
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/resul/bitbucket-satranc/MakeMove.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/bitbucket-satranc_MakeMove.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/bitbucket-satranc_MakeMove.cpp$(PreprocessSuffix): ../MakeMove.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/bitbucket-satranc_MakeMove.cpp$(PreprocessSuffix) "../MakeMove.cpp"

$(IntermediateDirectory)/bitbucket-satranc_Perft.cpp$(ObjectSuffix): ../Perft.cpp 
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/resul/bitbucket-satranc/Perft.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/bitbucket-satranc_Perft.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/bitbucket-satranc_Perft.cpp$(PreprocessSuffix): ../Perft.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/bitbucket-satranc_Perft.cpp$(PreprocessSuffix) "../Perft.cpp"

$(IntermediateDirectory)/bitbucket-satranc_Search.cpp$(ObjectSuffix): ../Search.cpp 
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/resul/bitbucket-satranc/Search.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/bitbucket-satranc_Search.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/bitbucket-satranc_Search.cpp$(PreprocessSuffix): ../Search.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/bitbucket-satranc_Search.cpp$(PreprocessSuffix) "../Search.cpp"

$(IntermediateDirectory)/bitbucket-satranc_Evaluation.cpp$(ObjectSuffix): ../Evaluation.cpp 
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/resul/bitbucket-satranc/Evaluation.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/bitbucket-satranc_Evaluation.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/bitbucket-satranc_Evaluation.cpp$(PreprocessSuffix): ../Evaluation.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/bitbucket-satranc_Evaluation.cpp$(PreprocessSuffix) "../Evaluation.cpp"

$(IntermediateDirectory)/bitbucket-satranc_Uci.cpp$(ObjectSuffix): ../Uci.cpp 
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/resul/bitbucket-satranc/Uci.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/bitbucket-satranc_Uci.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/bitbucket-satranc_Uci.cpp$(PreprocessSuffix): ../Uci.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/bitbucket-satranc_Uci.cpp$(PreprocessSuffix) "../Uci.cpp"

##
## Clean
##
clean:
	$(RM) -r ./Debug/


