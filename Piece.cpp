#include "Piece.h"

const std::array<uint32_t, NUM_PIECE_TYPE> Piece::material_value_{
    0, 100, 100, 325, 325, 325, 325, 550, 550, 1000, 1000, 30000, 30000};