#ifndef UCI_H
#define UCI_H

#include "definitions.h"

class Uci {
 public:
  static void Game();
  static void GetPosition(std::stringstream &ss);
  static void GetGo(std::stringstream &ss);
  static int InputWaiting();
};

#include "MakeMove.h"

#if defined(_WIN32)
#include "windows.h"
#elif defined(_WIN64)
#include "windows.h"
#else
#include "sys/time.h"
#include "sys/select.h"
#include "unistd.h"
#endif

#endif