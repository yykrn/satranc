#ifndef FEN_H_
#define FEN_H_

#include "definitions.h"

class Fen {
 public:
  static void ReadFen(const std::string &fen = INIT_FEN);
};
#include "Board.h"
#endif /* FEN_H_ */
