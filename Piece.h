#ifndef PIECE_H
#define PIECE_H

#include "definitions.h"

class Piece {
 private:
  static const std::array<uint32_t, NUM_PIECE_TYPE> material_value_;

 public:
  static bool IsPawn(const Piece_t t_piece) {
    return ((t_piece == piece::w_pawn) || (t_piece == piece::b_pawn));
  }
  static bool IsKing(const Piece_t t_piece) {
    return ((t_piece == piece::w_king) || (t_piece == piece::b_king));
  }
  static bool IsKnight(const Piece_t t_piece) {
    return ((t_piece == piece::w_knight) || (t_piece == piece::b_knight));
  }
  static bool IsRook(const Piece_t t_piece) {
    return ((t_piece == piece::w_rook) || (t_piece == piece::b_rook));
  }
  static bool IsQueen(const Piece_t t_piece) {
    return ((t_piece == piece::w_queen) || (t_piece == piece::b_queen));
  }
  static bool IsBishop(const Piece_t t_piece) {
    return ((t_piece == piece::w_bishop) || (t_piece == piece::b_bishop));
  }

  static bool IsMajor(const Piece_t piece_t) {
    if ((piece_t > piece::b_bishop) && (piece_t < piece::w_king)) return true;
    return false;
  }
  static bool IsMinor(const Piece_t piece_t) {
    if ((piece_t < piece::w_rook) && (piece_t > piece::b_pawn)) return true;
    return false;
  }
  static bool IsBig(const Piece_t piece_t) {
    if (piece_t > piece::b_pawn) return true;
    return false;
  }

  static bool IsSliding(const Piece_t piece_t) {
    if ((piece_t > piece::b_knight) && (piece_t < piece::w_king)) return true;
    return false;
  }

  static uint8_t GetColor(const Piece_t piece_t) {
    if ((piece_t == sq120::OUT) || (piece_t == piece::empty))
      return side::NONE;
    else if ((piece_t % 2) == 0)
      return side::black;
    return side::white;
  }

  static uint8_t GetType(const Piece_t piece_t) { return ((piece_t + 1) / 2); }
  static uint32_t GetValue(const Piece_t piece_t) {
    return Piece::material_value_[piece_t];
  }
};

#endif