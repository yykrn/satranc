#ifndef BOARD_H
#define BOARD_H

#include "History.h"
#include "Player.h"
#include "definitions.h"

class Board {
 public:
  static std::array<Piece_t, BOARD_SIZE> board_120_;
  static std::array<Piece_t *const, INTERNAL_BOARD_SIZE> board_64_;

  static Square_t enpas_sq_;

  static uint8_t side_to_move_;
  static uint8_t fifty_move_;

  static uint8_t castling_perm_;

  static std::array<std::vector<Square_t>, NUM_PIECE_TYPE> piece_list_;

  static std::array<Player, 2> player_;

  static uint64_t hash_key_;

  static Bit64_t all_pawn_bb_;

  static std::vector<History> history_;

  //////////////////////////////
  static void ResetBoard();

  static void InitHash();

  static void PrintBoard();
  static bool CalcHashKey();

  static void InitMaterial();

  static bool ControlData();

  static bool IsSamePos();

  static bool Is3Rep();

 private:
  static const std::array<Square_t, (BOARD_SIZE - INTERNAL_BOARD_SIZE)>
      offboard_index_;
};

#include "Hash.h"
#include "Piece.h"
#include "Fen.h"
#include "Bitset.h"
#include "Evaluation.h"
#endif  // !BOARD_H
