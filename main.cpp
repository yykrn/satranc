#include "Uci.h"
#include "Perft.h"
const std::string FEN1 =
    "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";  // D5 4865609
// ;D6
// 119060324
const std::string FEN2 = "n1n5/PPPk4/8/8/8/8/4Kppp/5N1N w - - 0 1";
const std::string FEN3 =
    "4k3/8/8/8/8/8/8/4K2R w K - 0 1";  // D5 133987 ;D6 764643
const std::string FEN4 =
    "4k3/8/8/8/8/8/8/R3K3 w Q - 0 1";  // D5 145232 ;D6 846648
const std::string FEN5 =
    "4k2r/8/8/8/8/8/8/4K3 w k - 0 1";  // D5 47635 ;D6 899442
const std::string FEN6 =
    "r3k3/8/8/8/8/8/8/4K3 w q - 0 1";  // D5 52710 ;D6 1001523
const std::string FEN7 =
    "4k3/8/8/8/8/8/8/R3K2R w KQ - 0 1";  // D5 532933 ;D6 2788982
const std::string FEN8 =
    "r3k2r/8/8/8/8/8/8/4K3 w kq - 0 1";  // D5 118882 ;D6 3517770
const std::string FEN9 =
    "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1";

int main() {
  Uci::Game();

  // Square::Init();
  // Fen::ReadFen(FEN9);
  // Perft::PerftTest(5);
  // system("pause");
  return 0;
}