#ifndef MOVEGEN_H
#define MOVEGEN_H

#include "definitions.h"
#include "Board.h"
#include "MakeMove.h"

struct MoveListEntry {
  Move_t move_;  // flags-->king cast.,queen cast.,pawn 2 sq.,en pas
  // capt.,?promotion,?capture
  // promoted p.->4 bit,captured p.->4 bit,piece itself->4 bit,to->7
  // bit,from->7
  // bit
  int64_t score_;
  MoveListEntry(){};
  MoveListEntry(Move_t move, int64_t score) : move_(move), score_(score){};
};

class MoveGen {
 private:
  static std::array<std::array<int64_t, NUM_PIECE_TYPE>, NUM_PIECE_TYPE>
      movcapt_;  // mvv/lva

 public:
  static std::vector<MoveListEntry> movelist_;

  static void SetQNormal(const Square_t from, const Square_t to,
                         const Piece_t pc);
  static void SetCNormal(const Square_t from, const Square_t to,
                         const Piece_t pc);

  static void AllMoves();
  static void AllWPawnMoves();
  static void AllWKnightMoves();
  static void AllWBishopMoves();
  static void AllWRookMoves();
  static void AllWQueenMoves();
  static void AllWKingMoves();

  static void AllBPawnMoves();
  static void AllBKnightMoves();
  static void AllBBishopMoves();
  static void AllBRookMoves();
  static void AllBQueenMoves();
  static void AllBKingMoves();

  static void OnlyCapPromMoves();

  static void OnlyTargetMoves(Move_t mv);

  static void SetWPawnQProm(const Square_t from, const Square_t to);
  static void SetWPawnCProm(const Square_t from, const Square_t to);

  static void SetWPawnCEnpas(const Square_t from, const Square_t to);

  static void SetWPawnQ2Sq(const Square_t from, const Square_t to);

  static void SetWKingCastl();

  static void SetWQueenCastl();

  ///////////////////////////////////////////////////////////////
  static void SetBPawnQProm(const Square_t from, const Square_t to);
  static void SetBPawnCProm(const Square_t from, const Square_t to);

  static void SetBPawnCEnpas(const Square_t from, const Square_t to);
  static void SetBPawnQ2Sq(const Square_t from, const Square_t to);

  static void SetBKingCastl();

  static void SetBQueenCastl();

  static void PrintMoveList();

  static Move_t IsInListByMask(Move_t move_mask, Move_t inp_move);
  static Move_t IsInList(Move_t inp_move);
  static bool IsLegal(Move_t move);

  static void Init();
};

#endif