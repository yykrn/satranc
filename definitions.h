#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include <iostream>
#include <cstdint>
#include <string>
//#include <cassert>
#include <bitset>
#include <vector>
#include <cstring>
#include <cstdio>
#include <sstream>
#include <random>
#include <iomanip>
#include <array>
#include <algorithm>
#include <chrono>
#include <unordered_map>

using Piece_t = uint8_t;
using Square_t = uint8_t;
using Bit64_t = std::bitset<64>;
using Move_t = uint32_t;

#define NODEBUG
#ifndef NODEBUG
#define ASSERT(condition)                                                 \
  {                                                                       \
    if (!(condition)) {                                                   \
      std::cerr << "Assertion failed at " << __FILE__ << ":" << __LINE__; \
      std::cerr << " inside " << __FUNCTION__ << std::endl;               \
      std::cerr << "Condition: " << #condition;                           \
      abort();                                                            \
    }                                                                     \
  }
#else
#define ASSERT(condition)
#endif

const uint8_t BOARD_SIZE = 120;
const uint8_t INTERNAL_BOARD_SIZE = 64;

const uint8_t NUM_PIECE_TYPE = 13;

const uint8_t SEARCH_DEPTH_LIMIT = 64;

const int16_t MATE_SCORE = 20000;

const int64_t PV_SC = 2100000000;
const int64_t CAP_SC = 2000000000;
const int64_t KIL1_SC = 1999999999;
const int64_t KIL2_SC = 1800000000;
const int64_t PROM_SC = 100000000;  // D�ZELT

const int64_t MB_IN_BYTES = 1048576;  // 1 MB in bytes

const std::string INIT_FEN =
    "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
//"r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1";
//"1r3rk1/1pnnq1bR/p1pp2B1/P2P1p2/1PP1pP2/2B3P1/5PK1/2Q4R w - - 0 1";
//"Q7/6k1/2R5/8/4N3/3K4/6P1/8 b - - 0 70";

namespace piece {
const Piece_t empty = 0;

const Piece_t w_pawn = 1;
const Piece_t w_knight = 3;
const Piece_t w_bishop = 5;
const Piece_t w_rook = 7;
const Piece_t w_queen = 9;
const Piece_t w_king = 11;

const Piece_t b_pawn = 2;
const Piece_t b_knight = 4;
const Piece_t b_bishop = 6;
const Piece_t b_rook = 8;
const Piece_t b_queen = 10;
const Piece_t b_king = 12;
}
namespace file {
const uint8_t A = 0;
const uint8_t B = 1;
const uint8_t C = 2;
const uint8_t D = 3;
const uint8_t E = 4;
const uint8_t F = 5;
const uint8_t G = 6;
const uint8_t H = 7;

const uint8_t NONE = UINT8_MAX;
}
namespace rank {
const uint8_t R1 = 7;
const uint8_t R2 = 6;
const uint8_t R3 = 5;
const uint8_t R4 = 4;
const uint8_t R5 = 3;
const uint8_t R6 = 2;
const uint8_t R7 = 1;
const uint8_t R8 = 0;

const uint8_t NONE = UINT8_MAX;
}
namespace side {
const uint8_t white = 0;
const uint8_t black = 1;
const uint8_t NONE = 2;
}

namespace sq120 {
const Square_t A8 = 21;
const Square_t B8 = 22;
const Square_t C8 = 23;
const Square_t D8 = 24;
const Square_t E8 = 25;
const Square_t F8 = 26;
const Square_t G8 = 27;
const Square_t H8 = 28;

const Square_t A7 = 31;
const Square_t B7 = 32;
const Square_t C7 = 33;
const Square_t D7 = 34;
const Square_t E7 = 35;
const Square_t F7 = 36;
const Square_t G7 = 37;
const Square_t H7 = 38;

const Square_t A6 = 41;
const Square_t B6 = 42;
const Square_t C6 = 43;
const Square_t D6 = 44;
const Square_t E6 = 45;
const Square_t F6 = 46;
const Square_t G6 = 47;
const Square_t H6 = 48;

const Square_t A5 = 51;
const Square_t B5 = 52;
const Square_t C5 = 53;
const Square_t D5 = 54;
const Square_t E5 = 55;
const Square_t F5 = 56;
const Square_t G5 = 57;
const Square_t H5 = 58;

const Square_t A4 = 61;
const Square_t B4 = 62;
const Square_t C4 = 63;
const Square_t D4 = 64;
const Square_t E4 = 65;
const Square_t F4 = 66;
const Square_t G4 = 67;
const Square_t H4 = 68;

const Square_t A3 = 71;
const Square_t B3 = 72;
const Square_t C3 = 73;
const Square_t D3 = 74;
const Square_t E3 = 75;
const Square_t F3 = 76;
const Square_t G3 = 77;
const Square_t H3 = 78;

const Square_t A2 = 81;
const Square_t B2 = 82;
const Square_t C2 = 83;
const Square_t D2 = 84;
const Square_t E2 = 85;
const Square_t F2 = 86;
const Square_t G2 = 87;
const Square_t H2 = 88;

const Square_t A1 = 91;
const Square_t B1 = 92;
const Square_t C1 = 93;
const Square_t D1 = 94;
const Square_t E1 = 95;
const Square_t F1 = 96;
const Square_t G1 = 97;
const Square_t H1 = 98;

const Square_t NONE = 100;
const Square_t OUT = 101;
}

namespace sq64 {
const Square_t A8 = 0;
const Square_t B8 = 1;
const Square_t C8 = 2;
const Square_t D8 = 3;
const Square_t E8 = 4;
const Square_t F8 = 5;
const Square_t G8 = 6;
const Square_t H8 = 7;

const Square_t A7 = 8;
const Square_t B7 = 9;
const Square_t C7 = 10;
const Square_t D7 = 11;
const Square_t E7 = 12;
const Square_t F7 = 13;
const Square_t G7 = 14;
const Square_t H7 = 15;

const Square_t A6 = 16;
const Square_t B6 = 17;
const Square_t C6 = 18;
const Square_t D6 = 19;
const Square_t E6 = 20;
const Square_t F6 = 21;
const Square_t G6 = 22;
const Square_t H6 = 23;

const Square_t A5 = 24;
const Square_t B5 = 25;
const Square_t C5 = 26;
const Square_t D5 = 27;
const Square_t E5 = 28;
const Square_t F5 = 29;
const Square_t G5 = 30;
const Square_t H5 = 31;

const Square_t A4 = 32;
const Square_t B4 = 33;
const Square_t C4 = 34;
const Square_t D4 = 35;
const Square_t E4 = 36;
const Square_t F4 = 37;
const Square_t G4 = 38;
const Square_t H4 = 39;

const Square_t A3 = 40;
const Square_t B3 = 41;
const Square_t C3 = 42;
const Square_t D3 = 43;
const Square_t E3 = 44;
const Square_t F3 = 45;
const Square_t G3 = 46;
const Square_t H3 = 47;

const Square_t A2 = 48;
const Square_t B2 = 49;
const Square_t C2 = 50;
const Square_t D2 = 51;
const Square_t E2 = 52;
const Square_t F2 = 53;
const Square_t G2 = 54;
const Square_t H2 = 55;

const Square_t A1 = 56;
const Square_t B1 = 57;
const Square_t C1 = 58;
const Square_t D1 = 59;
const Square_t E1 = 60;
const Square_t F1 = 61;
const Square_t G1 = 62;
const Square_t H1 = 63;

const Square_t NONE = 100;
}
namespace castling {
const uint8_t w_king_side = 8;   // 1000
const uint8_t w_queen_side = 4;  // 0100
const uint8_t b_king_side = 2;   // 0010
const uint8_t b_queen_side = 1;  // 0001

const uint8_t NONE = 0;
}
namespace to_str {
const std::array<char, NUM_PIECE_TYPE> pieces{'.', 'P', 'p', 'N', 'n', 'B', 'b',
                                              'R', 'r', 'Q', 'q', 'K', 'k'};
const std::array<char, 3> str_sides{'w', 'b', '-'};
}
namespace piece_type {
const Piece_t empty = 0;
const Piece_t pawn = 1;
const Piece_t knight = 2;
const Piece_t bishop = 3;
const Piece_t rook = 4;
const Piece_t queen = 5;
const Piece_t king = 6;
}

#endif
