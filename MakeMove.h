#ifndef MAKEMOVE_H
#define MAKEMOVE_H

#include "definitions.h"

class MakeMove {
 public:
  static uint32_t search_depth_;

  static void AddPiece(const Square_t sq, const Piece_t pc);
  static void RemovePiece(const Square_t sq);
  static void MovePiece(const Square_t from, const Square_t to);
  static bool DoMove(const Move_t move);
  static void UndoMove();
  static void UndoForCheck();

 private:
  static const std::array<uint8_t, BOARD_SIZE> perm_arr_;
};

#include "Board.h"
#include "Move.h"
#include "Search.h"

#endif