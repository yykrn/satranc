#include "Fen.h"

void Fen::ReadFen(const std::string &fen) {
  Board::ResetBoard();

  Square_t sq_num = sq64::A8;

  std::stringstream ss(fen);
  /////////////
  std::string fen_board;

  ss >> fen_board;

  for (unsigned int i = 0; i < fen_board.size() && sq_num < INTERNAL_BOARD_SIZE;
       i++) {
    unsigned int num_empty_sq = 0;
    Piece_t piece_t = piece::empty;

    switch (fen_board[i]) {
      case '/':
        continue;
      case 'p':
        piece_t = piece::b_pawn;
        break;
      case 'P':
        piece_t = piece::w_pawn;
        break;
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
        piece_t = piece::empty;
        num_empty_sq = std::atoi(&fen_board[i]);
        for (unsigned int j = 0; j < num_empty_sq; ++j)
          *Board::board_64_[sq_num + j] = piece::empty;
        sq_num += static_cast<Square_t>(num_empty_sq - 1);
        break;
      case 'r':
        piece_t = piece::b_rook;
        break;
      case 'R':
        piece_t = piece::w_rook;
        break;
      case 'n':
        piece_t = piece::b_knight;
        break;
      case 'N':
        piece_t = piece::w_knight;
        break;
      case 'b':
        piece_t = piece::b_bishop;
        break;
      case 'B':
        piece_t = piece::w_bishop;
        break;
      case 'k':
        piece_t = piece::b_king;
        break;
      case 'K':
        piece_t = piece::w_king;
        break;
      case 'q':
        piece_t = piece::b_queen;
        break;
      case 'Q':
        piece_t = piece::w_queen;
        break;
      default:
        std::cerr << "error in fen" << std::endl;
        return;
    }
    *Board::board_64_[sq_num++] = piece_t;
  }
  /////////////////
  char fen_side_to_move;

  ss >> fen_side_to_move;

  switch (fen_side_to_move) {
    case 'w':
      Board::side_to_move_ = side::white;
      break;
    case 'b':
      Board::side_to_move_ = side::black;
      break;
    default:
      std::cout << "error:fen side"
                << "\n";
      break;
  }
  ///////////////
  std::string fen_castle_perm;

  ss >> fen_castle_perm;

  // Board::castle_perm_ = 0;
  for (unsigned int i = 0; i < fen_castle_perm.size(); ++i) {
    switch (fen_castle_perm[i]) {
      case 'K':
        Board::castling_perm_ |= castling::w_king_side;
        break;
      case 'Q':
        Board::castling_perm_ |= castling::w_queen_side;
        break;
      case 'k':
        Board::castling_perm_ |= castling::b_king_side;
        break;
      case 'q':
        Board::castling_perm_ |= castling::b_queen_side;
        break;
      case '-':
        Board::castling_perm_ = castling::NONE;
        break;
      default:
        std::cout << "error:fen castle permission"
                  << "\n";
        break;
    }
  }
  //////////////
  static const std::unordered_map<std::string, Square_t>
      enpas_sq_to_boar120_index_ = {{"a6", sq120::A6},
                                    {"b6", sq120::B6},
                                    {"c6", sq120::C6},
                                    {"d6", sq120::D6},
                                    {"e6", sq120::E6},
                                    {"f6", sq120::F6},
                                    {"g6", sq120::G6},
                                    {"h6", sq120::H6},
                                    {"a3", sq120::A3},
                                    {"b3", sq120::B3},
                                    {"c3", sq120::C3},
                                    {"d3", sq120::D3},
                                    {"e3", sq120::E3},
                                    {"f3", sq120::F3},
                                    {"g3", sq120::G3},
                                    {"h3", sq120::H3},
                                    {"-", sq120::NONE}};

  std::string fen_enpas_sq;

  ss >> fen_enpas_sq;

  Board::enpas_sq_ = enpas_sq_to_boar120_index_.at(fen_enpas_sq);

  ////////////////
  std::string fen_fifty_move;
  ss >> fen_fifty_move;
  Board::fifty_move_ = std::atoi(fen_fifty_move.c_str());

  std::string fen_full_move;
  ss >> fen_full_move;
  unsigned int full_move = std::atoi(fen_full_move.c_str());

  if (Board::side_to_move_ == side::white)
    full_move = 2 * full_move - 2;
  else
    full_move = 2 * full_move - 1;

  while (full_move--) Board::history_.push_back(History());

  Board::InitMaterial();
  Board::CalcHashKey();
}