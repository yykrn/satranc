#ifndef TEST_H
#define TEST_H

#include "definitions.h"

class Test {
 public:
  static bool ForSquare120(unsigned int i) {
    if ((i <= sq120::H1) && (i >= sq120::A8)) return true;
    return false;
  }
  static bool ForSquare64(unsigned int i) {
    if ((i <= sq64::H1) && (i >= sq64::A8)) return true;
    return false;
  }
  static bool ForPiece(unsigned int i) {
    if ((i <= piece::b_king) && (i >= piece::w_pawn)) return true;
    return false;
  }
  static bool ForSide(unsigned int i) {
    if ((i <= side::black) && (i >= side::white)) return true;
    return false;
  }
  static bool ForEnpas(unsigned int i) {
    if (((i <= sq120::H3) && (i >= sq120::A3)) ||
        ((i <= sq120::H6) && (i >= sq120::A6)))
      return true;
    return false;
  }
};

#endif