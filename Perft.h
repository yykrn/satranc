#ifndef PERFT_H
#define PERFT_H

class Perft {
 private:
  static long leafNodes;

 public:
  static void PerftRec(int depth);
  static void PerftTest(int depth);
};

#include "MoveGen.h"

#endif