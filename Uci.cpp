#include "Uci.h"

// http://home.arcor.de/dreamlike/chess/
int Uci::InputWaiting() {
#if defined(__linux__)
  fd_set readfds;
  struct timeval tv;
  FD_ZERO(&readfds);
  FD_SET(fileno(stdin), &readfds);
  tv.tv_sec = 0;
  tv.tv_usec = 0;
  select(16, &readfds, 0, 0, &tv);

  return (FD_ISSET(fileno(stdin), &readfds));
#elif(defined(_WIN32) || defined(_WIN64))
  static int init = 0, pipe;
  static HANDLE inh;
  DWORD dw;

  if (!init) {
    init = 1;
    inh = GetStdHandle(STD_INPUT_HANDLE);
    pipe = !GetConsoleMode(inh, &dw);
    if (!pipe) {
      SetConsoleMode(inh, dw & ~(ENABLE_MOUSE_INPUT | ENABLE_WINDOW_INPUT));
      FlushConsoleInputBuffer(inh);
    }
  }
  if (pipe) {
    if (!PeekNamedPipe(inh, NULL, 0, NULL, &dw, NULL)) return 1;
    return dw;
  } else {
    GetNumberOfConsoleInputEvents(inh, &dw);
    return dw <= 1 ? 0 : dw;
  }
// DWORD nchar;
// PeekNamedPipe(GetStdHandle(STD_INPUT_HANDLE), NULL, 0, NULL, &nchar, NULL);
// return nchar;
#endif
}

void Uci::Game() {
  setvbuf(stdout, NULL, _IONBF, 0);
  setvbuf(stdin, NULL, _IONBF, 0);

  bool first = true;
  bool set_hash = true;
  while (true) {
    std::string message;
    if (!std::getline(std::cin, message)) continue;
    std::stringstream ss(message);
    std::string str;
    ss >> str;
    if (first) {
      if (str == "uci") {
        std::cout << "id name chessengine\n";
        std::cout << "id author ry\n";
        std::cout << "option name Hash type spin default 256 min 1 max 1024\n";
        std::cout << "uciok\n";
      } else if (str == "isready") {
        Square::Init();
        MoveGen::Init();
        if (set_hash) Search::Init();
        std::cout << "readyok\n";
        first = false;
      } else if (str == "setoption") {
        ss >> str;
        if (str == "name") {
          ss >> str;
          if (str == "Hash") {
            ss >> str;
            if (str == "value") {
              ss >> str;
              Search::Init(std::atoi(str.c_str()));
              set_hash = false;
            }
          }
        }
      }
    } else {
      if (str == "isready") {
        // Square::Init();
        std::cout << "readyok\n";
      } else if (str == "position") {
        Uci::GetPosition(ss);
      } else if (str == "ucinewgame") {
        Fen::ReadFen();
        Search::ResetTT();
        Search::history_heu_.fill(0);
        Search::killer_heu_.fill(KillerEntry{0, 0});
      } else if (str == "go") {
        Uci::GetGo(ss);
      } else if (str == "quit") {
        break;
      }
    }
  }
}
// position [fen <fenstring> | startpos ]  moves <move1> .... <movei>

void Uci::GetPosition(std::stringstream &ss) {
  std::string str;
  ss >> str;
  if (str == "startpos") {
    Fen::ReadFen();
  } else if (str == "fen") {
    std::string fen;
    ss >> str;
    fen = str;
    for (int i = 0; i < 5; ++i) {
      ss >> str;
      fen = fen + " " + str;
    }
    Fen::ReadFen(fen);
  }
  if (ss >> str) {
    if (str == "moves") {
      while (ss >> str) MakeMove::DoMove(Move::NotaToMove(str));
      Board::PrintBoard();
    }
  } else {
    Board::PrintBoard();
    return;
  }
}
void Uci::GetGo(std::stringstream &ss) {
  int64_t lefttime = 0;
  int64_t inctime = 0;
  int32_t leftmoves = 0;
  std::string str;
  while (ss >> str) {
    if (str == "infinite") {
      Search::ResetData(0);
      Search::IterativeSearch();
      return;
    }

    else if ((str == "wtime") && (Board::side_to_move_ == side::white)) {
      ss >> str;
      lefttime = std::stoi(str);
    }

    else if ((str == "btime") && (Board::side_to_move_ == side::black)) {
      ss >> str;
      lefttime = std::stoi(str);
    }

    else if ((str == "winc") && (Board::side_to_move_ == side::white)) {
      ss >> str;
      inctime = std::stoi(str);
    }

    else if ((str == "binc") && (Board::side_to_move_ == side::black)) {
      ss >> str;
      inctime = std::stoi(str);
    }

    else if (str == "movestogo") {
      ss >> str;
      leftmoves = std::stoi(str);
    }
  }
  uint64_t search_time;
  if (leftmoves == 0)
    search_time = lefttime;
  else
    search_time =
        (leftmoves * inctime) / (leftmoves + 1) + lefttime / leftmoves;

  Search::ResetData(search_time);
  Search::IterativeSearch();
}