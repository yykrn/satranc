#include "Evaluation.h"

// pst ler daha sonra değiştirilecek
// şu anki pst ler Tomasz Michniewski'nden alındı

const std::array<int8_t, INTERNAL_BOARD_SIZE> Evaluation::pst_pawn_{
    0,  0,  0,  0,   0,   0,  0,  0,  50, 50, 50,  50, 50, 50,  50, 50,
    10, 10, 20, 30,  30,  20, 10, 10, 5,  5,  10,  25, 25, 10,  5,  5,
    0,  0,  0,  20,  20,  0,  0,  0,  5,  -5, -10, 0,  0,  -10, -5, 5,
    5,  10, 10, -20, -20, 10, 10, 5,  0,  0,  0,   0,  0,  0,   0,  0};
const std::array<int8_t, INTERNAL_BOARD_SIZE> Evaluation::pst_knight_{
    -50, -40, -30, -30, -30, -30, -40, -50, -40, -20, 0,   0,   0,
    0,   -20, -40, -30, 0,   10,  15,  15,  10,  0,   -30, -30, 5,
    15,  20,  20,  15,  5,   -30, -30, 0,   15,  20,  20,  15,  0,
    -30, -30, 5,   10,  15,  15,  10,  5,   -30, -40, -20, 0,   5,
    5,   0,   -20, -40, -50, -40, -30, -30, -30, -30, -40, -50,
};
const std::array<int8_t, INTERNAL_BOARD_SIZE> Evaluation::pst_bishop_{
    -20, -10, -10, -10, -10, -10, -10, -20, -10, 0,   0,   0,   0,
    0,   0,   -10, -10, 0,   5,   10,  10,  5,   0,   -10, -10, 5,
    5,   10,  10,  5,   5,   -10, -10, 0,   10,  10,  10,  10,  0,
    -10, -10, 10,  10,  10,  10,  10,  10,  -10, -10, 5,   0,   0,
    0,   0,   5,   -10, -20, -10, -10, -10, -10, -10, -10, -20,
};

const std::array<int8_t, INTERNAL_BOARD_SIZE> Evaluation::pst_rook_{
    0,  0, 0, 0, 0, 0, 0, 0,  5,  10, 10, 10, 10, 10, 10, 5,
    -5, 0, 0, 0, 0, 0, 0, -5, -5, 0,  0,  0,  0,  0,  0,  -5,
    -5, 0, 0, 0, 0, 0, 0, -5, -5, 0,  0,  0,  0,  0,  0,  -5,
    -5, 0, 0, 0, 0, 0, 0, -5, 0,  0,  0,  5,  5,  0,  0,  0};
const std::array<int8_t, INTERNAL_BOARD_SIZE> Evaluation::pst_queen_{
    -20, -10, -10, -5, -5, -10, -10, -20, -10, 0,   0,   0,  0,  0,   0,   -10,
    -10, 0,   5,   5,  5,  5,   0,   -10, -5,  0,   5,   5,  5,  5,   0,   -5,
    0,   0,   5,   5,  5,  5,   0,   -5,  -10, 5,   5,   5,  5,  5,   0,   -10,
    -10, 0,   5,   0,  0,  0,   0,   -10, -20, -10, -10, -5, -5, -10, -10, -20};
const std::array<int8_t, INTERNAL_BOARD_SIZE> Evaluation::pst_king_{
    -30, -40, -40, -50, -50, -40, -40, -30, -30, -40, -40, -50, -50,
    -40, -40, -30, -30, -40, -40, -50, -50, -40, -40, -30, -30, -40,
    -40, -50, -50, -40, -40, -30, -20, -30, -30, -40, -40, -30, -30,
    -20, -10, -20, -20, -20, -20, -20, -20, -10, 20,  20,  0,   0,
    0,   0,   20,  20,  20,  30,  10,  0,   0,   10,  30,  20};

int32_t Evaluation::EvalBoard() {
  uint32_t material_w = Board::player_[side::white].material_;
  uint32_t material_b = Board::player_[side::black].material_;

  int32_t pst_value_w = Board::player_[side::white].pst_;
  int32_t pst_value_b = Board::player_[side::black].pst_;

  // for (Square_t sq : Board::piece_list_[piece::w_pawn])
  //  pst_value_w += (pst_pawn_[Square::Conv120To64(sq)]);

  // for (Square_t sq : Board::piece_list_[piece::b_pawn])
  //  pst_value_b += (pst_pawn_[Evaluation::IndexBlack(sq)]);

  // for (Square_t sq : Board::piece_list_[piece::w_knight])
  //  pst_value_w += (pst_knight_[Square::Conv120To64(sq)]);

  // for (Square_t sq : Board::piece_list_[piece::b_knight])
  //  pst_value_b += (pst_knight_[Evaluation::IndexBlack(sq)]);

  // for (Square_t sq : Board::piece_list_[piece::w_bishop])
  //  pst_value_w += (pst_bishop_[Square::Conv120To64(sq)]);

  // for (Square_t sq : Board::piece_list_[piece::b_bishop])
  //  pst_value_b += (pst_bishop_[Evaluation::IndexBlack(sq)]);

  // for (Square_t sq : Board::piece_list_[piece::w_rook])
  //  pst_value_w += (pst_rook_[Square::Conv120To64(sq)]);

  // for (Square_t sq : Board::piece_list_[piece::b_rook])
  //  pst_value_b += (pst_rook_[Evaluation::IndexBlack(sq)]);

  // for (Square_t sq : Board::piece_list_[piece::w_queen])
  //  pst_value_w += (pst_queen_[Square::Conv120To64(sq)]);

  // for (Square_t sq : Board::piece_list_[piece::b_queen])
  //  pst_value_b += (pst_queen_[Evaluation::IndexBlack(sq)]);

  /*for (Square_t sq : Board::piece_list_[piece::w_king])
    pst_value_w += (pst_king_[Square::Conv120To64(sq)]);

    for (Square_t sq : Board::piece_list_[piece::b_king])
    pst_value_b += (pst_king_[Evaluation::IndexBlack(sq)]);*/

  // int8_t factor_pst = 2;
  // int8_t factor_material = 8;

  int32_t score_diff_w = /*factor_pst * */ (pst_value_w - pst_value_b) +
                         /*factor_material * */ (material_w - material_b);

  if (Board::side_to_move_ == side::white) {
    return score_diff_w;
  } else {
    return -score_diff_w;
  }
}
int32_t Evaluation::GetPstForPiece(Square_t sq, Piece_t pc) {
  switch (pc) {
    case piece::w_pawn:
      return pst_pawn_[Square::Conv120To64(sq)];
    case piece::b_pawn:
      return pst_pawn_[Evaluation::IndexBlack(sq)];
    case piece::w_knight:
      return pst_knight_[Square::Conv120To64(sq)];
    case piece::b_knight:
      return pst_knight_[Evaluation::IndexBlack(sq)];
    case piece::w_bishop:
      return pst_bishop_[Square::Conv120To64(sq)];
    case piece::b_bishop:
      return pst_bishop_[Evaluation::IndexBlack(sq)];
    case piece::w_rook:
      return pst_rook_[Square::Conv120To64(sq)];
    case piece::b_rook:
      return pst_rook_[Evaluation::IndexBlack(sq)];
    case piece::w_queen:
      return pst_queen_[Square::Conv120To64(sq)];
    case piece::b_queen:
      return pst_queen_[Evaluation::IndexBlack(sq)];
    case piece::w_king:
      return 0; /*pst_king_[Square::Conv120To64(sq)];*/
    case piece::b_king:
      return 0; /*pst_king_[Evaluation::IndexBlack(sq)];*/
    default:
      std::cout << "error\n";
      return -1;
  }
}