#include "MakeMove.h"

uint32_t MakeMove::search_depth_{};

// from Vice
const std::array<uint8_t, BOARD_SIZE> MakeMove::perm_arr_{
    15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
    15, 15, 15, 14, 15, 15, 15, 12, 15, 15, 13, 15, 15, 15, 15, 15, 15, 15,
    15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
    15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
    15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
    15, 11, 15, 15, 15, 3,  15, 15, 7,  15, 15, 15, 15, 15, 15, 15, 15, 15,
    15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15};

void MakeMove::AddPiece(const Square_t sq, const Piece_t pc) {
  ASSERT(Test::ForSquare120(sq));
  ASSERT(Test::ForPiece(pc));

  Board::board_120_[sq] = pc;
  Board::hash_key_ ^= Hash::sqxpiece(sq);

  uint8_t t_side = Piece::GetColor(pc);

  ASSERT(Test::ForSide(t_side));

  if (Piece::IsPawn(pc)) {
    Board::player_[t_side].pawn_bb_.set(Square::Conv120To64(sq));
    Board::all_pawn_bb_.set(Square::Conv120To64(sq));
  }

  Board::piece_list_[pc].push_back(sq);
  Board::player_[t_side].material_ += Piece::GetValue(pc);
  Board::player_[t_side].pst_ += Evaluation::GetPstForPiece(sq, pc);
}

void MakeMove::RemovePiece(const Square_t sq) {
  ASSERT(Test::ForSquare120(sq));
  ASSERT(Test::ForPiece(Board::board_120_[sq]));

  Board::hash_key_ ^= Hash::sqxpiece(sq);
  Piece_t pc = Board::board_120_[sq];
  Board::board_120_[sq] = piece::empty;

  uint8_t t_side = Piece::GetColor(pc);

  if (Piece::IsPawn(pc)) {
    Board::player_[t_side].pawn_bb_.reset(Square::Conv120To64(sq));
    Board::all_pawn_bb_.reset(Square::Conv120To64(sq));
  }

  // Board::piece_list_[pc].erase(std::find(Board::piece_list_[pc].begin(),
  //                                       Board::piece_list_[pc].end(), sq));
  for (size_t i = Board::piece_list_[pc].size() - 1; i >= 0; --i)
    if (Board::piece_list_[pc][i] == sq) {
      Board::piece_list_[pc].erase(Board::piece_list_[pc].begin() + i);
      break;
    }
  Board::player_[t_side].material_ -= Piece::GetValue(pc);
  Board::player_[t_side].pst_ -= Evaluation::GetPstForPiece(sq, pc);
}

void MakeMove::MovePiece(const Square_t from, const Square_t to) {
  Board::hash_key_ ^= Hash::sqxpiece(from);
  Piece_t pc = Board::board_120_[from];
  ASSERT(Test::ForPiece(pc));
  Board::board_120_[from] = piece::empty;
  Board::board_120_[to] = pc;
  Board::hash_key_ ^= Hash::sqxpiece(to);

  uint8_t t_side = Piece::GetColor(pc);

  if (Piece::IsPawn(pc)) {
    Board::player_[t_side].pawn_bb_.reset(Square::Conv120To64(from));
    Board::player_[t_side].pawn_bb_.set(Square::Conv120To64(to));
    Board::all_pawn_bb_.reset(Square::Conv120To64(from));
    Board::all_pawn_bb_.set(Square::Conv120To64(to));
  }

  /**(std::find(Board::piece_list_[pc].begin(), Board::piece_list_[pc].end(),
                                                                                                  from)) = to;
                                                                                                  */
  for (size_t i = Board::piece_list_[pc].size() - 1; i >= 0; --i)
    if (Board::piece_list_[pc][i] == from) {
      Board::piece_list_[pc][i] = to;
      break;
    }
  Board::player_[t_side].pst_ -= Evaluation::GetPstForPiece(from, pc);
  Board::player_[t_side].pst_ += Evaluation::GetPstForPiece(to, pc);
}

bool MakeMove::DoMove(const Move_t move) {
  ASSERT(Board::ControlData());
  Square_t t_start = Move::GetStartSq(move);
  Square_t t_end = Move::GetEndSq(move);
  Piece_t t_piece = Move::GetMovedPiece(move);
  uint8_t t_side = Board::side_to_move_;

  Board::history_.emplace_back(Board::hash_key_, move, Board::castling_perm_,
                               Board::enpas_sq_, Board::fifty_move_);

  if (Board::side_to_move_ == side::white) {
    if (Move::IsEnPasCaptMove(move)) {
      MakeMove::RemovePiece(t_end + 10);
    } else if (Move::IsCastlMove(move)) {
      if (Move::IsCastlKingMove(move))
        MakeMove::MovePiece(sq120::H1, sq120::F1);
      else
        MakeMove::MovePiece(sq120::A1, sq120::D1);
    }
  } else {
    if (Move::IsEnPasCaptMove(move)) {
      MakeMove::RemovePiece(t_end - 10);
    } else if (Move::IsCastlMove(move)) {
      if (Move::IsCastlKingMove(move))
        MakeMove::MovePiece(sq120::H8, sq120::F8);
      else
        MakeMove::MovePiece(sq120::A8, sq120::D8);
    }
  }
  if (Board::enpas_sq_ != sq120::NONE)
    Board::hash_key_ ^= Hash::sqxpiece(Board::enpas_sq_);

  Piece_t captured = Move::GetCapturedPiece(move);
  ++Board::fifty_move_;

  if ((!Move::IsEnPasCaptMove(move)) && (captured != piece::empty)) {
    MakeMove::RemovePiece(t_end);
    Board::fifty_move_ = 0;
  }

  MakeMove::MovePiece(t_start, t_end);

  if (Piece::IsKing(Board::board_120_[t_end])) {
    Board::player_[Board::side_to_move_].king_sq_ = t_end;
  }

  if (Square::IsSqAttacked(Board::player_[Board::side_to_move_].king_sq_)) {
    MakeMove::UndoForCheck();
    return false;
  } else {
    Board::hash_key_ ^= Hash::castle_perm(Board::castling_perm_);

    Board::castling_perm_ &= MakeMove::perm_arr_[t_start];
    Board::castling_perm_ &= MakeMove::perm_arr_[t_end];

    Board::hash_key_ ^= Hash::castle_perm(Board::castling_perm_);

    Board::enpas_sq_ = sq120::NONE;

    ++MakeMove::search_depth_;

    if (Piece::IsPawn(t_piece)) {
      Board::fifty_move_ = 0;
      if (Move::IsPawn2SqMove(move)) {
        if (t_side == side::white) {
          Board::enpas_sq_ = t_start - 10;
        } else {
          Board::enpas_sq_ = t_start + 10;
        }
        Board::hash_key_ ^= Hash::sqxpiece(Board::enpas_sq_);
      }
    }
    Piece_t prPce = Move::GetPromotedPiece(move);
    if (prPce != piece::empty) {
      MakeMove::RemovePiece(t_end);
      MakeMove::AddPiece(t_end, prPce);
    }
    Board::side_to_move_ ^= 1;
    Board::hash_key_ ^= Hash::white_side();
    ASSERT(Board::ControlData());
    return true;
  }
}
void MakeMove::UndoMove() {
  ASSERT(Board::ControlData());
  --MakeMove::search_depth_;

  History old = Board::history_.back();
  Board::history_.pop_back();
  Square_t t_start = Move::GetStartSq(old.move_);
  Square_t t_end = Move::GetEndSq(old.move_);
  Piece_t t_piece = Move::GetMovedPiece(old.move_);

  Board::side_to_move_ ^= 1;

  if (Board::side_to_move_ == side::white) {
    if (Move::IsEnPasCaptMove(old.move_)) {
      MakeMove::AddPiece(t_end + 10, piece::b_pawn);
    } else if (Move::IsCastlMove(old.move_)) {
      if (Move::IsCastlKingMove(old.move_))
        MakeMove::MovePiece(sq120::F1, sq120::H1);
      else
        MakeMove::MovePiece(sq120::D1, sq120::A1);
    }
  } else {
    if (Move::IsEnPasCaptMove(old.move_)) {
      MakeMove::AddPiece(t_end - 10, piece::w_pawn);
    } else if (Move::IsCastlMove(old.move_)) {
      if (Move::IsCastlKingMove(old.move_))
        MakeMove::MovePiece(sq120::F8, sq120::H8);
      else
        MakeMove::MovePiece(sq120::D8, sq120::A8);
    }
  }

  MakeMove::MovePiece(t_end, t_start);

  if (Piece::IsKing(t_piece)) {
    Board::player_[Board::side_to_move_].king_sq_ = t_start;
  }

  Piece_t captured = Move::GetCapturedPiece(old.move_);
  if ((!Move::IsEnPasCaptMove(old.move_)) && (captured != piece::empty)) {
    MakeMove::AddPiece(t_end, captured);
  }

  if (Move::IsPromotionMove(old.move_)) {
    MakeMove::RemovePiece(t_start);
    MakeMove::AddPiece(
        t_start,
        Piece::GetColor(Move::GetPromotedPiece(old.move_)) == side::white
            ? piece::w_pawn
            : piece::b_pawn);
  }

  Board::castling_perm_ = old.castle_perm_;
  Board::fifty_move_ = old.fifty_move_;
  Board::enpas_sq_ = old.enpas_sq_;
  Board::hash_key_ = old.pos_key_;

  ASSERT(Board::ControlData());
}
void MakeMove::UndoForCheck() {
  History old = Board::history_.back();
  Board::history_.pop_back();
  Square_t t_start = Move::GetStartSq(old.move_);
  Square_t t_end = Move::GetEndSq(old.move_);
  Piece_t t_piece = Move::GetMovedPiece(old.move_);

  if (Board::side_to_move_ == side::white) {
    if (Move::IsEnPasCaptMove(old.move_)) {
      MakeMove::AddPiece(t_end + 10, piece::b_pawn);
    } else if (Move::IsCastlMove(old.move_)) {
      if (Move::IsCastlKingMove(old.move_))
        MakeMove::MovePiece(sq120::F1, sq120::H1);
      else
        MakeMove::MovePiece(sq120::D1, sq120::A1);
    }
  } else {
    if (Move::IsEnPasCaptMove(old.move_)) {
      MakeMove::AddPiece(t_end - 10, piece::w_pawn);
    } else if (Move::IsCastlMove(old.move_)) {
      if (Move::IsCastlKingMove(old.move_))
        MakeMove::MovePiece(sq120::F8, sq120::H8);
      else
        MakeMove::MovePiece(sq120::D8, sq120::A8);
    }
  }

  MakeMove::MovePiece(t_end, t_start);

  if (Piece::IsKing(t_piece)) {
    Board::player_[Board::side_to_move_].king_sq_ = t_start;
  }

  Piece_t captured = Move::GetCapturedPiece(old.move_);
  if ((!Move::IsEnPasCaptMove(old.move_)) && (captured != piece::empty)) {
    MakeMove::AddPiece(t_end, captured);
  }

  Board::castling_perm_ = old.castle_perm_;
  Board::fifty_move_ = old.fifty_move_;
  Board::enpas_sq_ = old.enpas_sq_;
  Board::hash_key_ = old.pos_key_;

  ASSERT(Board::ControlData());
}