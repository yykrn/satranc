#ifndef MOVE_H
#define MOVE_H

#include "definitions.h"

class Move {
 public:
  static Square_t GetStartSq(const Move_t move) { return (move & 0x7f); }
  static Square_t GetEndSq(const Move_t move) { return (move >> 7) & 0x7f; }
  static Piece_t GetMovedPiece(const Move_t move) { return (move >> 14) & 0xf; }
  static Piece_t GetCapturedPiece(const Move_t move) {
    return (move >> 18) & 0xf;
  }
  static Piece_t GetPromotedPiece(const Move_t move) {
    return (move >> 22) & 0xf;
  }
  static bool IsCaptureMove(const Move_t move) { return move & 0x3c0000; }
  static bool IsPromotionMove(const Move_t move) { return move & 0x3c00000; }
  static bool IsEnPasCaptMove(const Move_t move) { return move & 0x10000000; }
  static bool IsPawn2SqMove(const Move_t move) { return move & 0x20000000; }
  static bool IsCastlMove(const Move_t move) { return move & 0xc0000000; }
  static bool IsCastlQueenMove(const Move_t move) { return move & 0x40000000; }
  static bool IsCastlKingMove(const Move_t move) { return move & 0x80000000; }

  static void SetStartSq(Move_t &move, const Square_t sq) { move |= sq; }
  static void SetEndSq(Move_t &move, const Square_t sq) { move |= (sq << 7); }
  static void SetMovedPiece(Move_t &move, const Piece_t pc) {
    move |= (pc << 14);
  }
  static void SetCapturedPiece(Move_t &move, const Piece_t pc) {
    move |= (pc << 18);
  }
  static void SetPromotedPiece(Move_t &move, const Piece_t pc) {
    move |= (pc << 22);
  }
  static void SetEnPasCaptMove(Move_t &move) { move |= 0x10000000; }
  static void SetPawn2SqMove(Move_t &move) { move |= 0x20000000; }
  static void SetCastlQueenMove(Move_t &move) { move |= 0x40000000; }
  static void SetCastlKingMove(Move_t &move) { move |= 0x80000000; }
  static void PrintCoordNot(const Move_t move);
  static void PrintMove(const Move_t move);
  static Move_t NotaToMove(const std::string &str);
};

#include "Piece.h"
#include "MoveGen.h"

#endif  // !MOVE_H
