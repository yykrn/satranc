#ifndef BITSET_H
#define BITSET_H

#include "definitions.h"

class Bitset {
 private:
 public:
  static Square_t PopMostSign1Bit(Bit64_t &bitmap);
  static Square_t LeftMost1Bit(const Bit64_t &bitmap_t);
  static void DisplayBitset(const Bit64_t &bitmap);
};

#include "Test.h"

#endif